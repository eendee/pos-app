<?php

namespace App\Notifications;

use App\NotificationChannels\Sms\SmsChannel;
use App\NotificationChannels\Sms\SmsMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class PaymentNotification extends Notification
{
    use Queueable;

    protected $payment;
    protected $customer;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($customer, $payment)
    {
        $this->payment = $payment;
        $this->customer = $customer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    /**
     * Get the sms representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \App\Notifications\PaymentNotification
     */
    public function toSms($notifiable)
    {
        return SmsMessage::create("Dear {$this->customer->firstname}, your payment  of {$this->payment->amount} for Invoice No. {$this->payment->Invoice->code} has been received.  Ref:  {$this->payment->code}. Thanks for doing business with us.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
