<?php

namespace App\Helpers;

use Image;
use App\Model\File;
use Auth;


class SiteHelper
{

    public static $file_path = "uploads/";
    public static $pixel_200 = "_200x200";
    public static $pixel_300 = "_300";

    public function __construct()
    { }

    public static function CreateThumbnail($file)
    {
        // open an image file
        $path_parts = pathinfo(self::$file_path . $file);
        $img = Image::make($file);

        // now you are able to resize the instance
        $img->resize(200, 200);

        // finally we save the image as a new file
        $img->save(self::$file_path . $path_parts['filename'] . self::$pixel_200 . '.' . $path_parts['extension']);

        $img2 = Image::make($file);
        $img2->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save(self::$file_path . $path_parts['filename'] . self::$pixel_300 . '.' . $path_parts['extension']);
    }


    public static function UploadFile($file, $user_id, $custom_name = "")
    {

        $f = new File();
        $f->name = $file->getClientOriginalName();
        $f->mimetype = $file->getClientMimeType();
        $f->extension = $file->getClientOriginalExtension();
        $f->user_id = $user_id;
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        if ($custom_name != "") {
            $filename = pathinfo($custom_name, PATHINFO_FILENAME);
            $f->name = $custom_name;
        }

        $temp_name = str_replace(' ', '-', $filename) . '-' . date('YmdHis');
        $temp_name = self::Slug($temp_name);
        $f->url = $temp_name . '.' . $f->extension;



        $upload_success = $file->move(self::$file_path, $f->url);
        if ($upload_success) {
            if (strtoupper($f->extension) == 'JPG' || strtoupper($f->extension) == 'PNG') {
                self::CreateThumbnail(self::$file_path . $f->url);
            }
            $f->save();
            return $f->id;
        }

        return '0';
    }

    public static function Slug($string)
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }

    public static function MakeAuditObject($data, $action, $reason)
    {
        $user_id = 0;
        $user = Auth::user();
        if ($user) $user_id = $user->id;
        return [
            'user_id' => $user_id,
            'action' => $action,
            'reason' => $reason,
            'original_id' => $data->id,
            'audit_trail_created_at' => date("Y-m-d H:i:s")
        ];
    }

    public static function GetMonths()
    {
        $months = array();
        for ($m = 1; $m <= 12; $m++) {
            $month =  str_pad($m, (3 - strlen($m)), '0', STR_PAD_LEFT);
            $months[$month] = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
        }
        return $months;
    }

    public static function ShortenString($string, $length, $padding = "...")
    {
        if (strlen($string) <= $length) return $string;
        $string = substr($string, 0, $length);
        return $string . $padding;
    }
}
