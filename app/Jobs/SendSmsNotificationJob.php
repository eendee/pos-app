<?php

namespace App\Jobs;

use App\Notifications\PaymentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSmsNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    protected $customer;
    protected $payment;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($customer, $payment)
    {
        $this->customer = $customer;
        $this->payment = $payment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->customer->notify(
            new PaymentNotification($this->customer, $this->payment)
        );
    }
}
