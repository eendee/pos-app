<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Model\RoleMenuView;
use Auth;
use App\User;


class MenuComposer
{

    public $defaultSelect = array('' => 'Select');
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    { }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $user = Auth::user();
        $_user = null;
        $role_id = 0;
        if ($user) {
            $_user = User::where('id', $user->id)->first();
        }
        if ($_user != null) {
            $role_id = $_user->Role->id;
        }
        $items = RoleMenuView::BuildMenu();

        $specific = [];
        if (count($items)) {
            $specific = $items->where('RoleId', $role_id)->toArray();
        }

        $all = array();
        if (count($specific)) {
            foreach ($specific as $item) {
                $all[$item['MainMenu']][] = $item;
            }
        }

        $numberList1_10 = $this->defaultSelect + range(1, 10);

        $view->with('numberList1_10', $numberList1_10)->with('menuItems', $all);
    }
}
