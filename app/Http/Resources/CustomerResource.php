<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fullname' => $this->Fullname(),
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'middlenmae' => $this->middlename,
            'address' => $this->address,
            'email' => $this->email,
            'phone' => $this->phone,
            'code' => $this->code,
        ];
    }
}
