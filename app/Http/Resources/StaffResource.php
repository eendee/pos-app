<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StaffResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fullname' => $this->Fullname(),
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'middlename' => $this->middlename,
            'address' => $this->address,
            'email' => $this->email,
            'phone' => $this->phone,
            'code' => $this->code,
            'activated' => $this->activated,
            'user_id' => $this->user_id,
            'role_id' => $this->role_id,
            'role' => isset($this->Role) ? $this->Role->name : '',
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}
