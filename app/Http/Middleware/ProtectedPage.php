<?php

namespace App\Http\Middleware;

use Closure;
use App\RoleMenuView;
use App\User;

class ProtectedPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $user = auth()->user();
        $url = $request->path();

        $check = User::ValidatePageAccess($user, $url);

        if ($check) {

            return $next($request);
        } else {
            if (env('APP_ENV') == 'local') {
                return $next($request);
            } {
                abort(403, 'Unauthorized action.');
            }
        }
    }
}
