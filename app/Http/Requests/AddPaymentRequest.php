<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required',
            'customer' => 'required|array|min:1',
            'items' => 'required|array|min:1',
            'items.*' => 'required',
            'amount' => 'required',
            'discount' => 'required',
            'vat_included' => 'required',
            'vat_percentage' => 'required',
            'total' => 'required',
            'transaction_status' => 'required',
            'due_date' => 'required',
            'amount_paid' => 'required',
        ];
    }
}
