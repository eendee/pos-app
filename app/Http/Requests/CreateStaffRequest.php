<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateStaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email_rules = "required|unique:staff,email";
        $id = $this->route('id');
        if ($id != null) {
            $email_rules = $email_rules . ',' . $id;
        }
        return [
            'lastname' => 'required',
            'firstname' => 'required',
            'email' => $email_rules,
            'address' => 'required',
            'phone' => 'required',
            'role_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'role_id.required' => 'The role is required',
        ];
    }
}
