<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'transaction_status' => 'required',
            'amount' => 'required',
            'payment_channel_id' => 'required',
            'date_paid' => 'required',
            'transaction_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'transaction_status.required' => 'The Payment type is required',
            'payment_channel_id.required' => 'The Payment Channel is required',
            'date_paid.required' => 'The Payment Date is required',
        ];
    }
}
