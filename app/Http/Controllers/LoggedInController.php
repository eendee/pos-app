<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use \stdClass;

class LoggedinController extends Controller
{

    public $menu;
    public $meta;

    public function __construct()
    {

        $this->meta = new stdClass();
        $this->middleware('auth');
        $this->meta->active = '';
        $this->meta->title = '';
        View::share('meta', $this->meta);
    }
}
