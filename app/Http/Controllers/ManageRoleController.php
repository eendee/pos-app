<?php

namespace App\Http\Controllers;

use App\Model\Role;

use Illuminate\Http\Request;

class ManageRoleController extends LoggedinController
{
    //
    function __construct()
    {
        parent::__construct();
    }
    function index(Request $request)
    {
        $roles = Role::where('activated', '1')->get();
        return response($roles);
    }
}
