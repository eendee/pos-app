<?php

namespace App\Http\Controllers;

use App\Exports\GenericExport;
use App\Exports\InvoiceExport;
use App\Helpers\SiteHelper;
use App\Model\Customer;
use App\Model\InvoiceDetailView;
use App\Model\Item;
use App\Model\Payment;
use App\Model\PaymentDetailView;
use App\Model\Transaction;
use App\Model\TransactionStatus;
use Illuminate\Http\Request;
use View;
use Input;
use Excel;
use PDF;

class ReportsController extends LoggedinController
{
    public $years;
    public $months;
    public function __construct()
    {
        parent::__construct();
        $public_title = "Reports";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;

        $this->years = Transaction::GetDistinctYears();
        //dd($years);
        $this->months = SiteHelper::GetMonths();
        View::share('menu', $this->menu);
        View::share('months', $this->months);
        View::share('years', $this->years);
    }

    function index()
    {
        $this->meta->invoices = Transaction::count();
        $this->meta->customers = Customer::count();
        $this->meta->payments = Payment::count();
        $this->meta->paid_invoices = Transaction::where('transaction_status', '2')->count();

        $monthly_payments = PaymentDetailView::GetYearlySummary(date('Y'))->toArray();
        $monthly_invoices = InvoiceDetailView::GetYearlySummary(date('Y'))->toArray();

        //dd($this->meta);
        $months = range(1, 12);
        $data = array();
        foreach ($months as $month) {
            foreach ($monthly_invoices as $key => $value) {
                if (!isset($data[$month])) {
                    $data['i'][$month] = 0;
                }
                if ($month == $value['month_no']) {
                    $data['i'][$month] = $value['total'];
                }
            }

            foreach ($monthly_payments as $key => $value) {
                if (!isset($data[$month])) {
                    $data['p'][$month] = 0;
                }
                if ($month == $value['month_no']) {
                    $data['p'][$month] = $value['total'];
                }
            }
        }
        $bars = array();
        $items = Item::GetTopItems();
        if (count($items)) {
            foreach ($items as $key => $value) {
                $bars[$value->name] = $value->total;
            }
        }

        $this->meta->title = 'Dashboard';
        return view('reports/dashboard', compact('data', 'bars'));
    }

    function invoices()
    {

        $month = Input::get('month');
        $year = Input::get('year');
        $transaction_status = Input::get('transaction_status');
        $statuses = TransactionStatus::pluck('name', 'id')->toArray();
        $persist = compact('year', 'transaction_status', 'month');

        $results = null;
        if ($year != null && $month != null) {
            $data = Input::all();
            //dd($data);
            if (!in_array($month, array_keys($this->months)) || !in_array($year, array_keys($this->years))) {
                return redirect()->back()->withErrors('Year or month value is incorrect')->withInput();
            }
            $results = Transaction::GetReport($month, $year, $transaction_status);
        }
        $this->meta->title = 'Invoices Reports';
        return view('reports/invoices', compact('results', 'statuses', 'persist'));
    }

    function ExportInvoicesExcel()
    {

        $data = Input::All();

        $data = unserialize($data['s']);
        $selection = ['code', 'fullname', 'status', 'total', 'payments', 'created_at'];
        //dd($data);
        if ($data != null) {
            $data = InvoiceDetailView::GetReport($selection, $data);
            return Excel::download(new GenericExport($data, $selection), 'invoices.xlsx');
        }
    }

    function ExportInvoicesPDF()
    {

        $data = Input::All();
        $data = unserialize($data['s']);

        $results = Transaction::GetReport($data['month'], $data['year'], $data['transaction_status']);
        $pdf = PDF::loadView('pdf/invoices', compact('results'));
        return $pdf->download('report.pdf');
    }


    function payments()
    {
        $month = Input::get('month');
        $year = Input::get('year');

        $persist = compact('year', 'month');

        $results = null;
        if ($year != null && $month != null) {
            $data = Input::all();
            if (!in_array($month, array_keys($this->months)) || !in_array($year, array_keys($this->years))) {
                return redirect()->back()->withErrors('Year or month value is incorrect')->withInput();
            }
            $results = PaymentDetailView::GetReport($month, $year);
        }
        $this->meta->title = 'Payment Reports';
        return view('reports/payments', compact('results', 'persist'));
    }


    function ExportPaymentsExcel()
    {

        $data = Input::All();

        $data = unserialize($data['s']);
        $selection = ['code', 'invoice_code', 'amount', 'channel_name', 'created_at'];
        //dd($data);
        if ($data != null) {
            $data = PaymentDetailView::GetReportForExcel($selection, $data);
            return Excel::download(new GenericExport($data, $selection), 'payments.xlsx');
        }
    }

    function ExportPaymentsPDF()
    {

        $data = Input::All();
        $data = unserialize($data['s']);

        $results = PaymentDetailView::GetReport($data['month'], $data['year']);
        $pdf = PDF::loadView('pdf/payments', compact('results'));
        return $pdf->download('payments.pdf');
    }

    function yearlySummary()
    {

        $year = Input::get('year');

        $persist = compact('year', 'month');

        $results = null;
        if ($year != null) {
            $data = Input::all();
            if (!in_array($year, array_keys($this->years))) {
                return redirect()->back()->withErrors('Year value is incorrect')->withInput();
            }
            $results = PaymentDetailView::GetYearlySummary($year);
            $results2 = InvoiceDetailView::GetYearlySummary($year);
        }
        $this->meta->title = 'Yearly Summary Reports';
        return view('reports/summary', compact('results', 'results2', 'persist'));
    }
}
