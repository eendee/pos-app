<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use App\Model\Item;
use App\Model\Payment;
use App\Model\Setting;
use App\Model\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;
use  \stdClass;


class DashboardController extends LoggedinController
{
    public $menu;
    public $meta;
    public function __construct()
    {
        $this->meta = new stdClass();
        $this->middleware('auth')->except('CreateAccount', 'Activate');

        $public_title = "Home";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;
        $this->meta->title = '';


        View::share('menu', $this->menu);
        View::share('meta', $this->meta);
    }


    function Dashboard()
    {
        $user = Auth::user();
        $this->meta->title = 'Dashboard';
        $this->meta->invoices = Transaction::like('created_at', date('Y-m-d'))->count();
        $this->meta->customers = Customer::all()->count();
        $this->meta->services = Item::all()->count();
        $this->meta->payments = Payment::like('created_at', date('Y-m-d'))->count();

        $this->meta->company = Setting::GetOraganizationDetails();
        return view('account/dashboard')->with('user', $user)->with('menu', $this->menu);
    }
}
