<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Item;
use Input;
use Validator;
use App\Http\Resources\ItemResource;
use App\Http\Resources\SettingResource;
use App\Model\Setting;
use View;
use Auth;
use App\Helpers\SiteHelper;
use App\Model\File;

class SettingController extends LoggedinController
{
    private static $paginate = 20;

    public function __construct()
    {
        parent::__construct();
        $public_title = "Settings";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;
        View::share('menu', $this->menu);
    }

    function index()
    {
        $settings = Setting::OrderBy('id')->paginate(self::$paginate);
        $this->meta->title = 'Basic Settings';
        return view('setting/index', compact("settings"));
    }


    function save()
    {


        $data = Input::All();
        //dd($data);
        if (!isset($data['setting']) || count($data['setting']) <= 0) {
            return redirect()->back()->withErrors("Please enter the values correctly")->withInput();
        }

        //percentage must be set
        $percentage = $data['setting'][1]['value'];

        $int = 0;
        try {
            $int = intval($percentage);
            if ($int < 0 || $int > 100) {
                return redirect()->back()->withErrors("VAT percentage is out of range")->withInput();
            }
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors("VAT percentage must be a number")->withInput();
        }
        //save
        foreach ($data['setting'] as $key => $value) {
            Setting::where('id', $value['id'])->update(['value' => $value['value']]);
        }
        return Redirect('settings')->with('message', 'update successful');
    }

    public function delete()
    {

        // $id = Input::get('id');
        // Item::where('id', $id)->delete();
        // return Redirect('item')->with('message', 'update successful');
    }

    function GetSettingsJson()
    {
        return SettingResource::collection(Setting::all());
    }

    function editLogo()
    {
        $logo = Setting::GetLogoUrl();
        $this->meta->title = 'Company Logo';
        return view('setting/logo', compact("logo"));
    }

    function saveLogo()
    {
        $user = Auth::user();
        $data = Input::All();

        if (isset($data['file'])) {
            $file = array_get($data, 'file');
            $image_extentions = File::returnImageFileTypes();
            if (!in_array($file->guessClientExtension(), $image_extentions)) {
                return redirect()->back()->withErrors("Please attach a valid image file")->withInput();
            }
            $file_id = SiteHelper::UploadFile($file, $user->id, 'logo-file' . $user->id);
            Setting::where('id', 5)->update(array('value' => $file_id));
            return Redirect('settings/logo')->with('message', 'update successful');
        }
        return redirect()->back()->withErrors("Please attach a valid image file")->withInput();
    }
}
