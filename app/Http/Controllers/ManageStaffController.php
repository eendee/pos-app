<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStaffRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Resources\StaffResource;
use Illuminate\Http\Request;
use App\Model\Staff;
use App\Model\StaffPasswordReset;
use Symfony\Component\HttpFoundation\Response;
use Input;
use App\User;
use Auth;
use View;

class ManageStaffController extends LoggedinController
{
    //
    public function __construct()
    {
        parent::__construct();
        $public_title = "Staff";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;
        View::share('menu', $this->menu);
    }
    function index()
    {
        $this->meta->title = 'Manage Staff';
        return view('admin/vue-layout')->with('component', '<app-home></app-home>');
    }

    function AllStaffJson(Request $request)
    {
        return StaffResource::collection(Staff::all());
    }

    function OneStaffJson($id)
    {

        $staff = Staff::find($id);
        if ($staff) {
            return  new StaffResource($staff);
        }

        return response(['message' => 'staff not found'], Response::HTTP_NOT_FOUND);
    }

    function create(CreateStaffRequest $request)
    {
        //$user = Auth::user();
        $data = $request->all();
        $staff = Staff::CreateStaff($data);
        return response($staff);
    }
    function update(CreateStaffRequest $request)
    {
        //$user = Auth::user();
        $data = $request->all();
        $status = Staff::UpdateStaff($data);
        if ($status === 1) {
            return response('Update', Response::HTTP_ACCEPTED);
        } else {
            return response(0);
        }
    }

    public function delete(Request $request)
    {
        $staff = Staff::where('id', $request->id)->first();
        if ($staff && $staff->user_id == null) {
            $staff->delete();
            return response(null, Response::HTTP_NO_CONTENT);
        } else {
            return response('Staff Account Active', Response::HTTP_BAD_REQUEST);
        }
    }

    function PasswordReset()
    {
        //
        $this->meta->title = 'Reset Staff Password';
        return view('admin/staff/reset-password');
    }

    function SavePasswordReset(ResetPasswordRequest $request)
    {
        $data = Input::All();
        $user = Auth::user();
        //account does not exist
        $u = User::where('email', $data['email'])->first();
        if (!$u) {
            return redirect()->back()->with('error', "User account does not exist")->withInput();
        } else {
            $u->UpdatePassword($data['password']);
            //log reset
            $rest = StaffPasswordReset::create(
                array('user_id' => $user->id, 'staff_user_id' => $u->id, 'ipaddress' => request()->ip())
            );
            return view('pages/notice')->with('message', 'Password Updated')->with('messageType', 'success');
        }
    }
}
