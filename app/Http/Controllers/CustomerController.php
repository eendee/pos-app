<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRequest;
use App\Http\Resources\CustomerResource;
use Illuminate\Http\Request;
use App\Model\Customer;
use Input;
use View;
use Redirect;

class CustomerController extends LoggedinController
{
    //

    private static $paginate = 20;

    public function __construct()
    {
        parent::__construct();
        $public_title = "Customers";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;
        View::share('menu', $this->menu);
    }
    function index()
    {
        $customers = Customer::OrderByDesc('id')->paginate(self::$paginate);
        $this->meta->title = 'View Customers';
        return view('customer/index', compact("customers"));
    }

    function search()
    {
        $data = Input::all();
        $term = $data['term'];
        $this->meta->title = 'Search Customers';
        $customers = Customer::Search($term, self::$paginate);
        return view('customer/search-results', compact("customers", "term"));
    }

    public function edit()
    {

        $id = Input::get('id');

        $customer = new Customer();
        if ($id != null) {
            $customer = Customer::Find($id);
        }
        $this->meta->title = 'Add/Edit Customer';
        return view('customer/edit', compact("customer"));
    }
    function save(CustomerRequest $request)
    {
        $id = Input::get('id');
        $data = Input::all();
        if ($id == null) {
            $o = Customer::CreateCustomer($data);
        } else {
            $o = Customer::find($id);
            $o->update($data);
            $o->AddAudit('edit', 'edit');
        }
        return Redirect('customer')->with('message', 'update successful');
    }
    function delete()
    {
        $id = Input::get('id');
        if ($id == null) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }
        $c = Customer::find($id);
        if (!$c) {
            return view('pages/notice')->with('message', 'Customer not found');
        }
        // if ($c->Transactions->count() > 0) {
        //     return view('pages/notice')->with('message', 'Customer has transactions.');
        // }
        //dd($c);
        $c->AddAudit('delete', 'delete');
        Customer::find($id)->delete();
        return Redirect::to('customer')->with('message', 'Customer Successfully Deleted ');
    }

    function GetSingleCustomerJson()
    {
        $id = Input::get('id');
        $customer = Customer::find($id);
        if ($customer) {
            return  new CustomerResource($customer);
        }
    }
}
