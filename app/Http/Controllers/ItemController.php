<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Item;
use Input;
use Validator;
use App\Http\Resources\ItemResource;
use View;

class ItemController extends LoggedinController
{
    private static $paginate = 20;

    public function __construct()
    {
        parent::__construct();
        $public_title = "Transactions";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;
        View::share('menu', $this->menu);
    }

    function index()
    {
        $items = Item::where('source', 'direct')->OrderBy('name')->paginate(self::$paginate);
        $this->meta->title = 'Products/Services';
        return view('item/index', compact("items"));
    }

    function search()
    {
        $data = Input::all();
        $term = $data['term'];
        $items = Item::Search($term, self::$paginate);

        return view('item/search-results', compact("items", "term"));
    }

    public function edit()
    {

        $id = Input::get('id');
        $item = new Item();
        if ($id) {
            $item = Item::find($id);
        }
        $this->meta->title = 'Add/Edit Item';
        return view('item/edit', compact("item"));
    }

    function save()
    {
        $id = Input::get('id');

        $data = Input::All();

        $rules = Item::$rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($id == null) {
            $data = Item::CreateItem($data);
        } else {
            $o = Item::find($id);
            $o->AddAudit('edit', '');
            $o->update($data);
        }
        return Redirect('item')->with('message', 'update successful');
    }

    public function delete()
    {

        $id = Input::get('id');
        $item = Item::find($id);
        if ($item) {
            $item->AddAudit('delete', '');
        }
        Item::where('id', $id)->delete();
        return Redirect('item')->with('message', 'update successful');
    }

    function GetItemsJson()
    {
        return ItemResource::collection(Item::where('activated', '1')->where('item_type_id', '1')->where('source', '!=', 'custom')->get());
    }
}
