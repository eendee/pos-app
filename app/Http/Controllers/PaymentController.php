<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddPaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Http\Resources\NameIdResouce;
use App\Model\Audit\PaymentAudit;
use Illuminate\Http\Request;
use App\Model\Transaction;
use App\Model\Customer;
use App\Model\Item;
use App\Model\Payment;
use App\Model\PaymentDetailView;
use App\Model\PaymentChannel;
use App\Model\Setting;
use App\Model\TransactionItems;
use App\Model\TransactionStatus;
use Input;
use View;
use Auth;
use DB;

class PaymentController extends LoggedinController
{
    private static $paginate = 20;

    public function __construct()
    {
        parent::__construct();
        $public_title = "Transactions";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;
        View::share('menu', $this->menu);
    }

    function index()
    {
        $date = Input::get('date');
        $invoices = isset($date)  && $date != null ?
            Transaction::like('created_at', $date)->OrderByDesc('id')->with('Status')
            ->paginate(self::$paginate) : Transaction::OrderByDesc('id')->with('Status')
            ->paginate(self::$paginate);
        //dd($invoices);
        $this->meta->title = 'All Invoices';
        return view('invoice/index', compact("invoices"));
    }

    function search()
    {
        $data = Input::all();
        $term = $data['term'];
        $this->meta->title = 'Search Invoices';
        $invoices = Transaction::Search($term, self::$paginate);
        return view('invoice/search-results', compact("invoices", "term"));
    }

    function searchByCustomer()
    {
        $data = Input::all();
        $id = $data['id'];
        if (!$id) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }
        $customer = Customer::find($id);
        if (!$customer) {
            return view('pages/notice')->with('message', 'Customer not found');
        }
        $this->meta->title = 'Customer Invoices';
        $invoices = Transaction::where('customer_id', $id)->paginate(self::$paginate);
        return view('invoice/customer-invoices', compact("invoices", "customer"));
    }

    function payments()
    {
        $date = Input::get('date');
        $payments = isset($date)  && $date != null ?
            PaymentDetailView::like('created_at', $date)->OrderByDesc('id')
            ->paginate(self::$paginate) : PaymentDetailView::OrderByDesc('id')
            ->paginate(self::$paginate);
        $this->meta->title = 'All Payments';

        return view('payment/index', compact("payments"));
    }

    function SearchPayments()
    {
        $data = Input::all();
        $term = $data['term'];
        $this->meta->title = 'Search Payments';
        $payments = PaymentDetailView::Search($term, self::$paginate);
        return view('payment/search-results', compact("payments", "term"));
    }

    function viewInvoice()
    {
        $code = Input::get('code');
        $print = Input::get('print');
        if (!$code) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }
        $invoice = Transaction::where('code', $code)->with('Items')->with('Payments.Channel')->with('Customer')->first();
        if (!$invoice) {
            return view('pages/notice')->with('message', 'Item not found');
        }
        //dd($invoice);
        $details = Setting::GetOraganizationDetails();
        //dd($details);
        if ($print) {
            return view('invoice/print-invoice-page', compact("invoice", 'details'));
        }
        $this->meta->title = 'Invoice Details';
        return view('invoice/invoice-page', compact("invoice", 'details'));
    }

    function add()
    {
        $this->meta->title = 'Add invoice';
        return view('admin/vue-layout')->with('component', '<app-home></app-home>');
    }

    public function save(AddPaymentRequest $request)
    {
        $user = Auth::user();
        $data = Input::All();

        if (!count($data['items'])) {
            //
            return  response()->json(['errors' => ['items' => 'Transaction items cannot be empty']], 422);
        }

        if (
            !isset($data['customer']['firstname']) ||
            !isset($data['customer']['address']) ||
            !isset($data['customer']['phone'])
        ) {
            return  response()->json(['errors' => ['customer' => 'Customer details are incomplete']], 422);
        }

        $customer = Customer::where('phone', $data['customer']['phone'])->first();
        if ($customer) {
            if ($customer->id != $data['customer_id']) {
                return  response()->json(['errors' => ['customer' => "Phone number must be unique, " . $data['customer']['phone'] . " is already in use"]], 422);
            }
        }

        try {
            DB::beginTransaction();
            //is there a customer?
            $customer_id = $data['customer_id'];
            $customer = new Customer();
            if ($customer_id !== 0) {
                $customer = Customer::find($customer_id);
            } else {
                $customer = Customer::CreateCustomer($data['customer']);
            }
            //add transaction
            $data['fullname'] = $customer->Fullname();
            $data['customer_id'] = $customer->id;
            $data['transaction_type_id'] = 1;
            $data['initiator_id'] = $user->id;

            if ($data['transaction_status'] == "2") {
                $data['amount_paid'] = $data['total'];
            } elseif ($data['transaction_status'] == "1") {
                $data['amount_paid'] = 0;
            }


            $t = Transaction::CreateTransaction($data);

            //add the items
            $items = array();

            foreach ($data['items'] as $key => $val) {
                //if there are custom items then add them first
                $_id = (int) $val['item_id'];
                if ($_id <= 0) {
                    $new_item = Item::CreateItem($val, array('source' => 'custom'))->toArray();
                    $new_item['quantity'] = $val['quantity'];
                    $items[] = $new_item;
                } else {
                    $items[] = $val;
                }
            }

            foreach ($items as $key => $val) {
                TransactionItems::create($val + ['transaction_id' => $t->id]);
            }

            //handle payments here
            if (isset($data['payment_channel'])) {
                if ($data['transaction_status'] !== "1") {
                    $p = Payment::add([
                        'transaction_id' => $t->id,
                        'initiator_id' => $user->id,
                        'date_paid' => $data['date_paid'],
                        'amount' => $data['amount_paid'],
                        'payment_channel_id' => $data['payment_channel']
                    ]);
                    $p->AddAudit('create', 'create');
                }
            }
            DB::commit();
            return response($t);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    function update()
    {
        $id = Input::get('id');
        if (!$id) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }
        $invoice = Transaction::where('id', $id)->with('Items')->with('Customer')->first();
        if (!$invoice) {
            return view('pages/notice')->with('message', 'Invoice not found');
        }
        $this->meta->title = 'Add Payment to invoice';
        $payment_channles = PaymentChannel::all()->pluck('name', 'id')->toArray();
        $transaction_statuses = TransactionStatus::where('id', '>', '1')->get()->pluck('name', 'id')->toArray();
        $details = Setting::GetOraganizationDetails();
        return view('payment/edit', compact("invoice", "details", "payment_channles", "transaction_statuses"));
    }

    function updateStatus(UpdatePaymentRequest $request)
    {

        $user = Auth::user();
        $data = Input::All();
        $data['initiator_id'] = $user->id;
        $invoice = Transaction::find($data['transaction_id']);
        if ($invoice) {
            $balance = $invoice->GetBalance();

            if ($balance !== 0) {
                $p = Payment::add($data);
                $p->AddAudit('create', 'create');
                //do any updates to invoice, fetch again from db
                $invoice = Transaction::find($data['transaction_id']);
                $new_balance = $invoice->GetBalance();

                if ($new_balance <= 0) {
                    $invoice->transaction_status = 2;
                    $invoice->date_paid = $data['date_paid'];
                }
                $invoice->save();
                return Redirect("payment/invoice?code=" . $invoice->code)->with('message', 'Update successful');
            } else {
                return redirect()->back()->withErrors("Invoice has no balance")->withInput();
            }
        } else {
            return view('pages/notice')->with('message', 'Invoice not found');
        }
    }


    public function viewPaymentReceipt()
    {
        $code = Input::get('code');
        $print = Input::get('print');
        if (!$code) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }
        $payment = Payment::where('code', $code)->with('Invoice')->with('Channel')->with('Invoice.Customer')->first();
        if (!$payment) {
            return view('pages/notice')->with('message', 'payment not found');
        }
        //dd($payment);
        $details = Setting::GetOraganizationDetails();
        $invoice = $payment->Invoice;
        if ($print) {
            return view('payment/print-receipt-page', compact("payment", 'invoice', 'details'));
        }
        $this->meta->title = 'Payment Receipt';
        return view('payment/receipt-page', compact("payment", 'invoice', 'details'));
    }
    public function deletePaymentPrompt()
    {
        $id = Input::get('id');
        if (!$id) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }
        $payment = Payment::find($id);
        $invoice = $payment->Invoice;
        //dd($invoice);
        if (!$invoice) {
            return view('pages/notice')->with('message', 'Invoice not found');
        }

        if (!$payment) {
            return view('pages/notice')->with('message', 'Payment not found');
        }
        $this->meta->title = 'Delete entry';
        return view('payment/delete-payment', compact('payment', 'invoice'));
    }

    function deletePayment()
    {
        $data = Input::all();

        $id = Input::get('id');
        if (!$id) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }


        $p = Payment::find($id);
        $invoice = Transaction::where('id', $p->transaction_id)->first();
        if (!$invoice) {
            return view('pages/notice')->with('message', 'Invoice not found');
        }

        $p->AddAudit('delete', $data['reason']);
        $p->delete();
        //
        $payments_count = $invoice->Payments->count();

        if ($payments_count > 0) {
            $invoice->transaction_status = 3;
        } else {
            $invoice->transaction_status = 1;
        }
        if ($invoice->GetBalance() <= 0) {
            $invoice->transaction_status = 2;
        }

        $invoice->save();
        $invoice->AddAudit('update', "Payment edited: " . $data['reason']);

        return Redirect("payment/invoice?code=" . $invoice->code)->with('message', 'Delete successful');
    }

    public function deleteInvoicePrompt()
    {
        $id = Input::get('id');
        if (!$id) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }
        $invoice = Transaction::where('code', $id)->first();

        if (!$invoice) {
            return view('pages/notice')->with('message', 'Payment not found');
        }
        $this->meta->title = 'Delete invoice';
        return view('invoice/delete-invoice', compact('invoice'));
    }

    function deleteInvoice()
    {
        $data = Input::all();
        $id = Input::get('id');
        if (!$id) {
            return view('pages/notice')->with('message', 'Url is faulty');
        }
        $invoice = Transaction::where('id', $id)->first();
        if (!$invoice) {
            return view('pages/notice')->with('message', 'Invoice not found');
        }
        //log payments
        $payments = $invoice->Payments;
        if ($payments) {
            //we log and delete them
            foreach ($payments as $p) {
                $p->AddAudit('delete', $data['reason']);
                $p->delete();
            }
        }
        //log invoice
        $invoice->AddAudit('delete', $data['reason']);
        $invoice->delete();
        return Redirect("payment")->with('message', 'Delete successful');
    }

    function GetTransactionTypesJson()
    {
        return NameIdResouce::collection(TransactionStatus::where('activated', '1')->get());
    }

    function GetPaymentTypesJson()
    {
        return NameIdResouce::collection(PaymentChannel::where('activated', '1')->get());
    }


    function ViewPaymentsAudit()
    {
        $this->meta->title = 'Payments Audit';
        $payments =  PaymentAudit::where("action", "!=", "create")->OrderByDesc('created_at')->paginate(self::$paginate);
        return view('audit/payments', compact("payments"));
    }
}
