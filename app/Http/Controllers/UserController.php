<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\User;
use App\Model\Staff;
use Validator;
use Redirect;
use Exception;
use DB;
use Auth;
use Hash;
use View;
use App\Helpers\SiteHelper;
use App\Http\Resources\StaffResource;
use App\Model\File;
use App\Model\StaffPasswordReset;
use  \stdClass;

class UserController extends Controller
{
    //
    public $menu;
    public $meta;
    public function __construct()
    {
        $this->meta = new stdClass();
        $this->middleware('auth')->except('CreateAccount', 'Activate');

        $public_title = "My Account";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;
        $this->meta->title = '';


        View::share('menu', $this->menu);
        View::share('meta', $this->meta);
    }



    function ChangePassword()
    {
        $user = Auth::user();
        $this->meta->title = 'Change Password';
        return view('account/change-password')->with('user', $user);
    }

    public function SavePassword()
    {

        $user = Auth::user();
        $data = Input::All();
        //dd($data);
        if ($data['newpassword'] !== $data['newpasswordconfirm']) {
            return redirect()->back()->with('message', 'the new password and the confirm password fields must be the same value');
        }
        if (Hash::check($data['oldpassword'], $user->getAuthPassword())) {
            $user->password = Hash::make($data['newpassword']);
            $user->save();
            return view('account/change-password-success')->with('user', $user);
        } else {
            return redirect()->back()->with('message', 'What you supplied as the current password did not match what we have as your current password');
        }
    }

    function Activate()
    {
        return view('user/activate-account');
    }

    function CreateAccount()
    {

        $data = Input::All();
        //dd($data);
        $validator = Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if (!isset($data['email'])) {
            return redirect()->back()->with('error', "please enter the registration number")->withInput();
        }
        $email = $data['email'];
        $existing = User::where('email', $email)->first();
        if ($existing != null) {
            return redirect()->back()->with('error', "Account already exists")->withInput();
        }
        $staff = Staff::where('email', $email)->first();
        if (!isset($staff)) {
            return redirect()->back()->with('error', "We did not find the record.")->withInput();
        }
        if (!$staff->activated) {
            return redirect()->back()->with('error', "Your account has not been activated.")->withInput();
        } else {
            $user_array = array('email' => $email, 'name' => $email);
            DB::beginTransaction();
            $a = User::CreateUser($user_array + array('password' => $data['password'], 'role_id' => $staff->role_id));
            $staff->user_id = $a->id;
            $staff->update();

            DB::commit();
            return redirect('login')->with('message', "Account creation successful, you can now login");
        }
    }


    function profile()
    {
        $user = Auth::user();
        $staff = Staff::where('user_id', $user->id)->first();
        $this->meta->title = 'My Profile';
        return view('account/profile', compact('user', 'staff'));
    }

    function passwordResets()
    {
        $user = Auth::user();
        $staff = Staff::where('user_id', $user->id)->first();
        $logs = StaffPasswordReset::where('staff_user_id', $staff->user_id)->get();

        $this->meta->title = 'Password reset log';
        return view('account/password-resets', compact('user', 'staff', 'logs'));
    }

    public function updateProfile()
    {
        $user = Auth::user();
        $data = Input::All();
        $staff =  Staff::where('user_id', $user->id)->first();

        $file_id = null;
        if (isset($data['file'])) {
            $file = array_get($data, 'file');
            $image_extentions = File::returnImageFileTypes();
            if (!in_array($file->guessClientExtension(), $image_extentions)) {
                return redirect()->back()->withErrors("Please attach a valid image file")->withInput();
            }
            $file_id = SiteHelper::UploadFile($file, $user->id, 'profile_' . $user->id);
            $staff->picture_id = $file_id;
        }

        $staff->lastname = $data['lastname'];
        $staff->middlename = $data['middlename'];
        $staff->firstname = $data['firstname'];
        $staff->save();

        return Redirect::to('account/dashboard')->with('message', 'Update Successful');
    }


    function GetUserJson()
    {
        $user = Auth::user();
        $staff = Staff::where('user_id', $user->id)->first();
        if ($staff) {
            return new StaffResource($staff);
        }
    }
}
