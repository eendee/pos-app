<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Menu;
use App\Model\MenuCategory;
use App\Model\RoleMenu;
use App\Model\Role;
use Input;
use Validator;
use View;

class MenuController extends LoggedinController
{

    public function __construct()
    {
        parent::__construct();
        $public_title = "Menus";
        $this->menu[$public_title] = 'active';
        $this->meta->active = $this->menu['title'] = $public_title;
        View::share('menu', $this->menu);
    }


    public function index()
    {

        $menus = Menu::with('MenuCategory')->OrderBy('menu_category_id')->OrderBy('name')->get();
        $this->meta->title = 'View Menus';
        //dd($menus);
        return view('admin/menu/index', compact("menus"))->with('menu', $this->menu);
    }


    public function edit()
    {
        $id = Input::get('id');

        $menu_ = new Menu();
        if ($id != null) {
            $menu_ = Menu::Find($id);
        }
        $this->meta->title = "Add Menu";
        $mainMenus = MenuCategory::GetAll();
        return view('admin/menu/edit', compact('mainMenus', 'menu_'));
    }

    function save()
    {
        $id = Input::get('id');

        $data = Input::All();
        if (!isset($data['menu_category_id'])) {
            return redirect()->back()->with('error', "Please select the menu category")->withInput();
        }
        $rules = Menu::$rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($id == null) {
            $data = Menu::Create($data);
        } else {
            $o = Menu::find($id);
            $o->update($data);
        }
        return Redirect('admin/menu')->with('message', 'update successful');
    }

    function roles()
    {
        $this->meta->title = "Roles";
        $roles = Role::All();
        return view('admin/menu/roles', compact('roles'));
    }

    function RoleMenus()
    {
        $id = Input::get('id');
        if ($id == null) {
            return view('front/notice')->with('message', 'Url is faulty');
        }
        $role = Role::find($id);
        $existing = RoleMenu::GetRoleMenus($id)->pluck('menu_id')->toArray();
        $menus = Menu::All();
        $this->meta->title = 'Menus In role';
        return view('admin/menu/role_menu', compact('menus', 'role', 'existing'));
    }

    function UpdateMenuInRole()
    {
        $data = Input::All();
        //dd($data);
        if (isset($data['menu'])) {
            if (count($data['menu'])) {
                //clear entry
                RoleMenu::MassDeleteByRoleId($data['role_id']);
                foreach ($data['menu'] as $key => $value) {
                    RoleMenu::Create(array('role_id' => $data['role_id'], 'menu_id' => $key));
                }
            }
        }

        return Redirect("admin/roles/menus?id=" . $data['role_id'])->with('message', 'Update successful');
    }
}
