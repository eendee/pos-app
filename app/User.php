<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use App\Model\RoleMenuView;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public  function  Staff()
    {
        return $this->hasOne('App\Model\Staff', 'user_id', 'id');
    }

    public function Role()
    {
        return $this->belongsTo('App\Model\Role');
    }
    public function UpdatePassword($str)
    {
        $this->password = Hash::make($str);
        $this->save();
    }
    public static function CreateUser($data)
    {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role_id' => $data['role_id'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function GetProfilePicture()
    {
        try {
            return $this->Staff->GetProfilePicture();
        } catch (\Throwable $th) {
            //throw $th;
        }
        return "img/user_icon.png";
    }

    public static function ValidatePageAccess($user, $url)
    {

        $user = User::find($user->id);
        $items = RoleMenuView::GetRoleMenuUrl($user->role_id);
        $check = in_array('/' . $url, $items);

        return $check;
    }
}
