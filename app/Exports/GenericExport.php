<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class GenericExport implements FromQuery, WithHeadings
{
    use Exportable;

    private $query;
    private $headings;
    public function __construct($data, $headings)
    {
        $this->query = $data;
        $this->headings = $headings;
    }

    public function headings(): array
    {
        return $this->headings;
    }

    public function query()
    {
        return $this->query;
    }
}
