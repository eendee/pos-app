<?php

namespace App\Exports;

use App\Model\Transaction;
use Maatwebsite\Excel\Concerns\FromCollection;

class InvoiceExport implements FromCollection
{

    public $collection;
    /**
     * @return \Illuminate\Support\Collection
     */
    #
    public function collection()
    {
        return Transaction::all();
    }
}
