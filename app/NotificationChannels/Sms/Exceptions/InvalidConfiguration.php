<?php

namespace App\NotificationChannels\Sms\Exceptions;

use Exception;

class InvalidConfiguration extends Exception
{
    public static function configurationNotSet(): self
    {
        return new static('In order to send notifications via SMS you need to add credentials in the `sms` key of `config.services`.');
    }
}
