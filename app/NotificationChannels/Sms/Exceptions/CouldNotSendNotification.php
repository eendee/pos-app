<?php

namespace App\NotificationChannels\Sms\Exceptions;

use Exception;

class CouldNotSendNotification extends Exception
{
    public static function serviceRespondedWithAnError(string $error): self
    {
        return new static("SMS service responded with an error: {$error}'");
    }
}
