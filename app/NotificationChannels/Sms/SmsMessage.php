<?php


namespace App\NotificationChannels\Sms;


class SmsMessage
{
    /** @var string */
    protected $body;
    /** @var string */
    protected $originator;
    /** @var string */
    protected $reference;


    private function __construct(string $body = '')
    {
        $this->body($body);
    }
    /**
     * @param string $body
     * @return $this
     */
    public function body(string $body)
    {
        $this->body = trim($body);
        return $this;
    }
    /**
     * @param string|int $originator
     * @return $this
     * @throws InvalidMessage
     */
    public function originator($originator)
    {
        if (empty($originator) || strlen($originator) > 11) {
            throw InvalidMessage::invalidOriginator($originator);
        }
        $this->originator = (string) $originator;
        return $this;
    }
    /**
     * @param string $reference
     * @return $this
     * @throws InvalidMessage
     */
    public function reference(string $reference)
    {
        if (empty($reference) || strlen($reference) > 32 || !ctype_alnum($reference)) {
            throw InvalidMessage::invalidReference($reference);
        }
        $this->reference = $reference;
        return $this;
    }


    /**
     * @param string $body
     * @return static
     */
    public static function create($body = ''): self
    {
        return new static($body);
    }

    public function  getReference()
    {
        return $this->reference;
    }
    public function  getBody()
    {
        return $this->body;
    }
    public function  getOriginator()
    {
        return $this->originator;
    }
}
