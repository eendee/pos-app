<?php

namespace App\NotificationChannels\Sms;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class SmsClient
{
    const GATEWAY_URL = 'https://portal.nigeriabulksms.com/api/';

    /**
     * @param SmsMessage $message
     * @param string $recipient
     * @throws Exception
     */
    public function send(SmsMessage $message, string $recipient)
    {
        if (!is_string($recipient)) {
            exit(); //handle later;
        }
        $guzzle = new Client();
        $sms['username'] = config('services.sms.username');
        $sms['password'] = config('services.sms.password');
        $sms['message'] = $message->getBody();
        $sms['sender'] = config('services.sms.sender');
        $sms['mobiles'] = $recipient;


        $query_string = 'username=' . $sms['username'] . '&' .
            'password=' . $sms['password'] . '&' .
            'message=' . $sms['message'] . '&' .
            'sender=' . $sms['sender'] . '&' .
            'mobiles=' . $recipient;

        $url = static::GATEWAY_URL . '?' . $query_string;
        try {
            $response = $guzzle->get($url);
            $code = $response->getStatusCode();
            if ($code == "200") {
                $response_string = $response->getBody()->getContents();
                $body = json_decode($response_string);
                if (isset($body) && isset($body->status) &&  $body->status == "OK") {
                    //sms was sent
                } else {
                    //log error
                    Log::error($response_string);
                    //throw new \Exception($response_string);
                }
            } else {
                //throw new \Exception('Failed to connect to sms server ' . $response->getReasonPhrase());
                //commenting this out so it fails silently
            }
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
        };
    }
}
