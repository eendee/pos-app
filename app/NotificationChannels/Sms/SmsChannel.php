<?php

namespace App\NotificationChannels\Sms;

use App\NotificationChannels\Sms\SmsClient;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class SmsChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $recipient = $notifiable->routeNotificationFor('Sms');

        if (!$recipient) {
            return;
        }
        $message = $notification->toSms($notifiable);
        if (is_string($message)) {
            $message = SmsMessage::create($message);
        }

        $client = new SmsClient();

        $client->send($message, $recipient);
    }
}
