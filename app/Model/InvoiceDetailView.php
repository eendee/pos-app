<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class InvoiceDetailView extends BaseModel
{
    protected $table = "vwinvoices";


    public static function GetReport($selection, $conditions)
    {
        if ($conditions['transaction_status'] == null) {
            unset($conditions['transaction_status']);
        }
        $query = DB::table('vwinvoices')->select($selection)
            ->where($conditions)->OrderBy('created_at');
        return $query;
    }

    public static function  GetYearlySummary($year)
    {
        $results = InvoiceDetailView::SelectRaw('DISTINCT year, MONTHNAME(date_paid) as month,  MONTH(date_paid) as month_no,  COUNT(id) as totalCount, sum(total) as total')
            ->whereRaw("year= $year")->GroupBy('year', 'month')->OrderBy('date_paid')->get();
        return $results;
    }
}
