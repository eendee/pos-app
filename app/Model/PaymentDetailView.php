<?php

namespace App\Model;

use DB;

use Illuminate\Database\Eloquent\Model;

class PaymentDetailView extends BaseModel
{
    protected $table = "vwpayments";


    public static function Search($term, $pagination)
    {
        return PaymentDetailView::where('code', 'like', "%$term%")
            ->orWhere('fullname', 'like', "%$term%")->orWhere('invoice_code', 'like', "%$term%")->paginate($pagination);
    }

    public static function GetReportForExcel($selection, $conditions)
    {
        $query = DB::table('vwpayments')->select($selection)
            ->where($conditions)->OrderBy('created_at');
        return $query;
    }

    public static function GetReport($month, $year)
    {

        $results = PaymentDetailView::SelectRaw('*')
            ->whereRaw("YEAR(date_paid)= $year")
            ->whereRaw("month(date_paid)= $month")->get();
        return $results;
    }

    public static function  GetYearlySummary($year)
    {
        $results = PaymentDetailView::SelectRaw('DISTINCT year, MONTHNAME(date_paid) as month,  MONTH(date_paid) as month_no, COUNT(id) as totalCount, sum(amount) as total')
            ->whereRaw("year= $year")->GroupBy('year', 'month')->OrderBy('date_paid')->get();
        return $results;
    }
}
