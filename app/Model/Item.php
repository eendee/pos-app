<?php

namespace App\Model;

use App\Helpers\SiteHelper;
use App\Model\Audit\ItemAudit;
use Illuminate\Database\Eloquent\Model;
use  App\Model\Interfaces\AuditInterface;
use DB;

class Item extends AuditableModel
{


    protected $fillable = ['name', 'amount', 'source', 'activated'];
    protected $auditclass = ItemAudit::class;

    public static $rules = array(
        'name'    => 'required',
        'amount'  => 'required|numeric',
    );




    public static function Search($term, $pagination)
    {
        return Item::where('name', 'like', "%$term%")
            ->where('source', 'direct')->paginate($pagination);
    }

    public static function CreateItem($data, $source = array())
    {
        $i = Item::Create($data + $source);
        $i->AddAudit('create', 'First creation');
        return $i;
    }

    public static function  GetTopItems($year = null, $limit = null)
    {
        $limit != null | $limit = 5;
        $year != null | $year = date('Y');
        $results = DB::Select('SELECT name, count(name), sum(quantity) as total  FROM vwpaymentitems where YEAR(date_paid)=' . $year . ' GROUP BY name  ORDER BY sum(quantity) desc Limit ' . $limit);
        return $results;
    }
}
