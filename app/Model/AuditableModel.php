<?php

namespace App\Model;

use  App\Model\Interfaces\AuditInterface;
use App\Helpers\SiteHelper;

class AuditableModel extends BaseModel implements AuditInterface
{
    protected $auditclass;
    public function AuditTrail()
    {
        return  $this->auditclass;
    }

    public function AddAudit($action, $reason)
    {
        $data = SiteHelper::MakeAuditObject($this, $action, $reason) + $this->toArray();

        foreach ($data as $key => $value) {
            if (is_array($value)) unset($data[$key]);
        }
        $this->AuditTrail()::create($data);
    }
}
