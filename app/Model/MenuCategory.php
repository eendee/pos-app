<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MenuCategory extends BaseModel
{
    public static function GetAll()
    {
        return MenuCategory::All('id', 'name')->pluck('name', 'id')->toArray();
    }
}
