<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StaffPasswordReset extends BaseModel
{
    //
    protected $guarded = [];

    public function Initiator()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function Staff()
    {
        return $this->belongsTo('App\User', 'staff_user_id', 'id');
    }
}
