<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use stdClass;

class Setting extends BaseModel
{
    protected $fillable = ['value'];
    public $timestamps = false;
    public static $rules = array(

        'value'  => 'Required'
    );

    public static function GetOraganizationDetails()
    {
        $all = Setting::OrderBy('id')->get();
        try {
            $obj = new stdClass();
            $obj->name = $all[2]->value;
            $obj->address = $all[3]->value;
            $obj->bank = $all[5]->value;
            $obj->logo = self::GetLogoUrl();
            $obj->phone = $all[6]->value;
            $obj->email = $all[7]->value;
            $obj->website = $all[8]->value;
        } catch (\Throwable $th) {
            //throw $th;
        }
        return $obj;
    }
    public static function GetLogoUrl()
    {
        return File::find(Setting::find(5)->value)->ReturnUrl();
    }
}
