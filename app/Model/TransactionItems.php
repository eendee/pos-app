<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransactionItems extends BaseModel
{
    public $timestamps = false;
    protected $fillable = ['name', 'transaction_id', 'amount', 'quantity', 'item_id'];
}
