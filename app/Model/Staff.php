<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Staff extends BaseModel
{
    protected $fillable = [
        'lastname',
        'middlename',
        'firstname',
        'email',
        'address',
        'phone',
        'activated',
        'role_id',
    ];

    public function GetFillables()
    {
        return $this->fillable;
    }

    public  function  File()
    {
        return $this->belongsTo('App\Model\File', 'picture_id', 'id');
    }

    public function Role()
    {
        return $this->belongsTo('App\Model\Role');
    }
    function Fullname()
    {
        return $this->lastname . ' ' .  $this->firstname . ' ' . $this->middlename;
    }

    public static function  CreateStaff($data)
    {
        foreach ($data as $key => $elem) {
            if (is_bool($elem)) {
                $data[$key] = ($elem) ? '1' : '0';
            }
        }
        $staff = Staff::create($data);
        $staff->GenerateCode();
        $staff->save();
        return $staff;
    }

    public function GetProfilePicture()
    {
        $picture = "img/user_icon.png";
        if (isset($this->picture_id)) {
            $picture = 'uploads/' . $this->file->url;
        }

        return $picture;
    }
    public function GenerateCode()
    {
        $this->code = "SP/$this->role_id/" . str_pad($this->id, (7 - strlen($this->id)), '0', STR_PAD_LEFT);
    }

    static function UpdateStaff($data)
    {
        foreach ($data as $key => $elem) {
            if (is_bool($elem)) {
                $data[$key] = ($elem) ? '1' : '0';
            }
        }
        try {
            $_staff = new Staff();
            $allowed = $_staff->GetFillable();
            unset($data['email']);
            $persist = array_intersect_key(array_filter($data, 'strlen'), array_flip($allowed));
            $staff = Staff::find($data['id']);
            if ($staff) {
                if (isset($data['role_id'])) {
                    User::where('id', $staff->role_id)->update(array('role_id' => $data['role_id']));
                }
            }
            Staff::where('id', $data['id'])->update($persist);

            return 1;
        } catch (\Throwable $th) {
            //return $th;
            throw $th;
        }
    }
}
