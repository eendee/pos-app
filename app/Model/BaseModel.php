<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use  App\Model\Interfaces\AuditInterface;
use App\Helpers\SiteHelper;

class BaseModel extends Model
{
    public  function scopeLike($query, $field, $value)
    {
        return $query->where($field, 'LIKE', "%$value%");
    }
}
