<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends BaseModel
{
    protected $fillable = ['role_id', 'menu_id'];

    public static function GetRoleMenus($roleId)
    {
        return RoleMenu::where('role_id', $roleId)->get();
    }

    public static function MassDeleteByRoleId($id)
    {
        RoleMenu::where('role_id', $id)->delete();
    }
}
