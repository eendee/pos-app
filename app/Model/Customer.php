<?php

namespace App\Model;

use App\Model\Audit\CustomerAudit;
use Illuminate\Notifications\Notifiable;

class Customer extends AuditableModel
{
    use Notifiable;

    protected $fillable = [
        'lastname',
        'middlename',
        'firstname',
        'email',
        'address',
        'phone',
    ];
    protected $auditclass = CustomerAudit::class;

    public  function  Transactions()
    {
        return $this->hasMany('App\Model\Transaction');
    }

    public function GetFillables()
    {
        return $this->fillable;
    }
    public function GenerateCode()
    {
        $this->code = "SP/C/" . str_pad($this->id, (7 - strlen($this->id)), '0', STR_PAD_LEFT);
    }

    function Fullname()
    {
        return trim($this->lastname . ' ' .  $this->firstname . ' ' . $this->middlename);
    }


    public function routeNotificationForSms()
    {
        return $this->phone;
    }

    public static function Search($term, $pagination)
    {
        return Customer::where('firstname', 'like', "%$term%")
            ->orWhere('lastname', 'like', "%$term%")->orWhere('phone', 'like', "%$term%")->paginate($pagination);
    }

    public static function CreateCustomer($data)
    {
        $c = Customer::Create($data);
        $c->GenerateCode();
        $c->save();
        $c->AddAudit('create', 'create');
        return $c;
    }
}
