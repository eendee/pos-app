<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class RoleMenuView extends BaseModel
{
    //
    protected $table = 'vwmenuinrole';


    public static function BuildMenu()
    {
        $results = Cache::remember('results', 1, function () {
            return RoleMenuView::where('displayed', '1')->where('menu_category_id', '!=', '6')
                ->orderByDesc('CategoryPriority')->orderByDesc('priority')->get();
            // return RoleMenuView::where('displayed', '1')
            //     ->orderByDesc('CategoryPriority')->orderByDesc('priority')->get();
        });
        return $results;
    }

    public static function GetRoleMenuUrl($role_id)
    {
        $results = RoleMenuView::where('RoleId', $role_id)->pluck('url')->toArray();
        return $results;
    }
}
