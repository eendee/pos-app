<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class File extends BaseModel
{

    protected $fillable = ['name', 'mimetype', 'extension', 'url'];
    public static function returnImageFileTypes()
    {
        return array('jpeg', 'jpg', 'png', 'gif');
    }
    public function ReturnThumbnail()
    {
        $url = str_replace('.' . $this->extension, '', $this->url);
        $url = 'uploads/' . $url . '_200x200' . '.' . $this->extension;
        return $url;
    }

    public function ReturnUrl()
    {
        $url = str_replace('.' . $this->extension, '', $this->url);
        $url = 'uploads/' . $url . '.' . $this->extension;
        return $url;
    }
}
