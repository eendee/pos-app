<?php

namespace App\Model;

use App\Model\Audit\TransactionAudit;
use Illuminate\Database\Eloquent\Model;

class Transaction extends AuditableModel
{
    protected $fillable = [
        'code',
        'fullname',
        'customer_id',
        'initiator_id',
        'amount',
        'discount',
        'vat_included',
        'vat_percentage',
        'total',
        'description',
        'transaction_type_id',
        'transaction_status',
        'due_date',
        'date_paid',
    ];

    protected $auditclass = TransactionAudit::class;
    public function GetFillables()
    {
        return $this->fillable;
    }
    public function Customer()
    {
        return $this->belongsTo('App\Model\Customer');
    }

    public function Items()
    {
        return $this->hasMany('App\Model\TransactionItems');
    }

    public function Payments()
    {
        return $this->hasMany('App\Model\Payment');
    }

    public function GenerateCode()
    {
        $this->code = "SP/INV/" . str_pad($this->id, (10 - strlen($this->id)), '0', STR_PAD_LEFT);
    }

    public function CalculateVAT()
    {
        return ((float) $this->amount - (float) $this->discount)  * (float) $this->vat_percentage / 100;
    }

    function GetBalance()
    {
        return (float) $this->total - $this->Payments->sum('amount');
    }
    public function Status()
    {
        return $this->belongsTo('App\Model\TransactionStatus', 'transaction_status', 'id');
    }

    public function GetTransactionBadge()
    {
        switch ($this->transaction_status) {
            case 1:
                return 'danger';
                break;
            case 2:
                return 'success';
                break;
            case 3:
                return 'warning';
                break;

            default:
                return 'info';
                break;
        }
    }


    public static function Search($term, $pagination)
    {
        return Transaction::where('description', 'like', "%$term%")
            ->orWhere('fullname', 'like', "%$term%")->orWhere('code', 'like', "%$term%")->paginate($pagination);
    }
    public static function CreateTransaction($data)
    {
        if ($data['transaction_status'] != "1") {
            $data['payment_date'] = null;
        }
        $t = Transaction::create($data);
        $t->GenerateCode();
        $t->save();
        $t->AddAudit('create', 'create');
        return $t;
    }

    public static function GetDistinctYears()
    {
        $results =  Transaction::SelectRaw('DISTINCT YEAR(date_paid) as year')->get()->keyBy('year')->toArray();
        $array = array();
        foreach ($results as $key => $value) {
            $array[$key] = $value['year'];
        }
        return $array;
    }


    public static function GetReport($month, $year, $status = null)
    {

        $results = Transaction::SelectRaw('*')
            ->whereRaw("YEAR(date_paid)= $year")
            ->whereRaw("month(date_paid)= $month");
        if ($status != null && trim($status) != '') {
            $results->where('transaction_status', $status);
        }
        return $results->with('Status')->get();
    }
}
