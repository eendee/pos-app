<?php

namespace App\Model\Audit;

use Illuminate\Database\Eloquent\Model;

class TransactionAudit extends Model
{
    protected $guarded = ['id'];
    protected $table = 'audit_trail_transactions';


    public function Customer()
    {
        return $this->hasMany('App\Model\Audit\CustomerAudit', 'original_id', 'customer_id');
    }
}
