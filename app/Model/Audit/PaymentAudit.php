<?php

namespace App\Model\Audit;

use Illuminate\Database\Eloquent\Model;

class PaymentAudit extends Model
{
    protected $guarded = ['id'];
    protected $table = 'audit_trail_payments';

    public function Initiator()
    {
        return $this->belongsTo('App\User', 'initiator_id', 'id');
    }

    public function Invoices()
    {
        return $this->hasMany('App\Model\Audit\TransactionAudit', 'original_id', 'transaction_id');
    }
}
