<?php

namespace App\Model\Audit;

use Illuminate\Database\Eloquent\Model;

class CustomerAudit extends Model
{
    protected $guarded = ['id'];
    protected $table = 'audit_trail_customers';

    function Fullname()
    {
        return trim($this->lastname . ' ' .  $this->firstname . ' ' . $this->middlename);
    }
}
