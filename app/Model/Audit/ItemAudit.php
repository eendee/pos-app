<?php

namespace App\Model\Audit;

use Illuminate\Database\Eloquent\Model;

class ItemAudit extends Model
{
    protected $guarded = ['id'];
    protected $table = 'audit_trail_items';
}
