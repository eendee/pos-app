<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Menu extends BaseModel
{
    protected $fillable = ['name', 'url', 'priority', 'menu_category_id', 'activated', 'displayed'];
    public $timestamps = false;
    public static $rules = array(
        'name'    => 'required',
        'url'  => 'required',
        'priority'         => 'required',
        'menu_category_id' => 'required'
    );
    //
    public  function  MenuCategory()
    {
        return $this->belongsTo('App\Model\MenuCategory');
    }
}
