<?php

namespace App\Model;

use App\Jobs\SendSmsNotificationJob;
use App\Model\Audit\PaymentAudit;

class Payment extends AuditableModel
{

    protected $fillable = ['amount', 'payment_channel_id', 'transaction_id', 'initiator_id', 'date_paid'];
    protected $auditclass = PaymentAudit::class;

    public static function boot()
    {
        parent::boot();


        self::created(function ($model) {
            //we generate the code here
            $model->code = 'SP/PM/' . $model->transaction_id . '/' . $model->payment_channel_id . '/' . str_pad($model->id, (10 - strlen($model->id)), '0', STR_PAD_LEFT);
            $model->save();
        });
    }

    public function Channel()
    {
        return $this->belongsTo('App\Model\PaymentChannel', 'payment_channel_id', 'id');
    }
    public function Initiator()
    {
        return $this->belongsTo('App\User', 'initiator_id', 'id');
    }
    public function Invoice()
    {
        return $this->belongsTo('App\Model\Transaction', 'transaction_id', 'id');
    }

    public static function Add($data)
    {
        $payment = Payment::create($data);
        $customer = $payment->Invoice->Customer;
        if ($customer) {

            dispatch(new SendSmsNotificationJob($customer, $payment))->delay(now()->addSeconds(20));
        }
        return $payment;
    }
}
