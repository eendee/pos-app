@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')
@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Reports </h2>
            
            <div class="row">
                <div class="col-sm-12">
                    {!! Form::model(null,array('method' => 'get', 'class'=>'form-horizontal')) !!}
                        <table class="table">
                            <tr>
                                <td>Year</td>
                                <td>{{Form::select('year',[''=>'Select']+ $years,isset($_GET['year'])?$_GET['year']:'',array('class'=>'form-control','required'=>'required'))}}</td>
                            </tr>
                    
                            <tr>
                                <td>
                                    
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </td>
                            </tr>
                        </table>
                    {{ Form::token() }}
                    {!! Form::close() !!}
                   
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <h5>Invoices for {{$_GET['year']}}</h5>
                            @if(isset($results2) &&$results2!=null)
                            <div>
                                @if(isset($results2)&& count($results2))
                                    
                                    <table class="table table-striped table-bordered table-condensed">
                                        <tr>
                                            <th>Month</th>
                                            <th>Count</th>
                                            <th>Total Amount</th>
                                        </tr>
                                        @foreach($results2 as $p)
                                            <tr>
                                                <td>
                                                    {{$p->month}}
                                                </td>
                                                <td>
                                                    {{$p->totalCount}}
                                                </td>
                                                <td>
                                                    {{$p->total}}
                                                </td> 
                                            </tr>
                                        @endforeach
                                    </table>
                                    @else
                                    <p class="alert alert-danger">No records found</p>
                                @endif
                            </div>
                            @endif
                        </div>
                        <div class="col-sm-6">
                            @if(isset($results) &&$results!=null)
                                <div>
                                    <h5>Payments for {{$_GET['year']}}</h5>
                                    @if(isset($results)&& count($results))
                                        <table class="table table-striped table-bordered table-condensed">
                                            <tr>
                                                <th>Month</th>
                                                <th>Count</th>
                                                <th>Total Amount</th>
                                            </tr>
                                            @foreach($results as $p)
                                                <tr>
                                                    <td>
                                                        {{$p->month}}
                                                    </td>
                                                    <td>
                                                        {{$p->totalCount}}
                                                    </td>
                                                    <td>
                                                        {{$p->total}}
                                                    </td> 
                                                </tr>
                                            @endforeach
                                        </table>
                                        @else
                                        <p class="alert alert-danger">No records found</p>
                                    @endif
                                </div>
                            @endif
                        </div>
                       
                    
                </div>
            </div>
        </div>
    </div>

@stop