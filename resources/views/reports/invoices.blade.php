@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Reports </h2>
            
            <div class="row">
                <div class="col-sm-12">
                    {!! Form::model(null,array('method' => 'get', 'class'=>'form-horizontal')) !!}
                        <table class="table">
                            <tr>
                                <td>Year</td>
                                <td>{{Form::select('year',[''=>'Select']+ $years,isset($_GET['year'])?$_GET['year']:'',array('class'=>'form-control','required'=>'required'))}}</td>
                            </tr>
                            <tr>
                                <td>Month</td>
                                <td>{{Form::select('month',[''=>'Select']+ $months,isset($_GET['month'])?$_GET['month']:'',array('class'=>'form-control','required'=>'required'))}}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>{{Form::select('transaction_status',[''=>'All']+ $statuses,isset($_GET['transaction_status'])?$_GET['transaction_status']:'',array('class'=>'form-control'))}}</td>
                            </tr>
                            <tr>
                                <td>
                                    
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </td>
                            </tr>
                        </table>
                    {{ Form::token() }}
                    {!! Form::close() !!}
                   
                </div>

                <div class="col-md-12">
                    <div>
                        @if(isset($results) &&$results!=null)
                            <div>
                                @if(isset($results)&& count($results))
                                    <table class="table table-striped table-bordered table-condensed">
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Total</th>
                                            <th>Status</th>
                                        </tr>
                                        @foreach($results as $i)
                                            <tr>
                                                <td>
                                                    {{$i->code}}
                                                </td>
                                                <td>
                                                    {{$i->fullname}}
                                                </td>
                                                <td>
                                                    {{$i->date_paid}}
                                                </td> 
                                                <td>
                                                    {{$i->total}}
                                                </td> 
                                                 <td>
                                                    {{$i->Status->name}}
                                                </td> 
                                            </tr>
                                        @endforeach
                                    </table>
                                    <a class="btn btn-default" href="{{url('/reports/invoices/export/excel').'?s='.serialize($persist)}}">Export Excel</a>
                                    <a class="btn btn-default" href="{{url('/reports/invoices/export/pdf').'?s='.serialize($persist)}}">Download PDF</a>
                                @else
                                    <p class="alert alert-danger">No records found</p>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop