@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

<?php 
	$url = 'customer/edit';
	$method='POST';
	if(isset($customer)&&$customer->id>=1){
		$url="customer/edit?id=$customer->id";
		$method="POST";
	}
?>

@section('page')


<div class="card">
<div class="card-body">

    <div class="row">
      

        <div class="col-sm-8">
              <h2> Customer Details </h2>
            {!! Form::model($customer,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
        
            <table class="table">

                <tr>
                    <td>Firstname/Company name</td>
                    <td>{{Form::text('firstname',null,array('class'=>'form-control' , 'required'=>'required'))}}</td>
                </tr>
                <tr>
                    <td>Lastname (Optional)</td>
                    <td>{{Form::text('lastname',null,array('class'=>'form-control',))}}</td>
                </tr>
                <tr>
                    <td>Middlename (Optional)</td>
                    <td>{{Form::text('middlename',null,array('class'=>'form-control'))}}</td>
                </tr>
                
                <tr>
                    <td>Phone No </td>
                    <td>{{Form::text('phone',null,array('class'=>'form-control' , 'required'=>'required'))}}</td>
                </tr>
                <tr>
                    <td>Address </td>
                    <td>{{Form::textarea('address',null,array('class'=>'form-control' ,'rows'=>'2', 'required'=>'required'))}}</td>
                </tr>
                <tr>
                    <td>Email (Optional) </td>
                    <td>{{Form::text('email',null,array('class'=>'form-control' ))}}</td>
                </tr>
              
                
                

                <tr>
                    <td></td>
                    <td>
                    {{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
                    </td>
                </tr>
            </table>

            {{ Form::token() }}
            {!! Form::close() !!}
        </div>
        <div class="col-sm-4">
        
            @if(isset($customer)&&$customer->id>=1)
                <div class="alert alert-info">
                            @if(isset($customer->Transactions) && $customer->Transactions->count()!=0)
                                <div>
                                Number of Invoices:  <span class="badge badge-info">{{$customer->Transactions->count()}}</span>
                                </div>
                            @else
                                <div>
                                    <p>No invoices for this customer yet</p>
                                    <form method="post" action="{{url('customer/delete')}}" onsubmit="return confirm('Are you sure you want to delete this item?');">
                                    {{ Form::token() }}
                                                <input type="hidden" name="id" value="{{$customer->id}}">
                                                <input type="submit" value="delete" class="btn btn-sm btn-danger">
                                            {!! Form::close() !!}
                                </div>
                            @endif
                
                </div>
            @endif
        </div>
    </div>
</div>
</div>
@stop