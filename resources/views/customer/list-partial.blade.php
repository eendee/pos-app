<div class="">
    <div class="col-sm-6 pull-right">
        <form method="get" action="{{url('/customer/search')}}">
   
            <div class="input-group">
             {{Form::text('term',isset($term)?$term:'',array('class'=>'form-control',  'required'=>"required",'placeholder'=>'search by name or phone'))}}
              <span class="input-group-btn">
                {{ Form::submit('Search',array('class'=>'btn btn-primary pull-right'))}}
              </span>
            </div><!-- /input-group -->
        </form>
    </br>
    </div>
    <table class="table table-striped ">
        <tr>
            <th>Name</th>
            <th>Code</th>
            <th></th>
        </tr>

        <?php  $count=0;?>
        @if(isset($customers))
            @foreach($customers as $s)

                <?php  $count++;?>
                <tr>
                   
                    <td>
                      {{$s->Fullname()}}
                    </td>    
                      <td>
                      {{$s->code}}
                    </td>  
                    <td>
                        <a class="btn btn-success btn-sm" title="View Invoices" href="{{url('payment/customer/invoices?id='.$s->id)}}">
                           <i class="fa fa-file-text"></i>
                        </a>
                        <a class="btn btn-danger btn-sm" title="Add Invoice" href="{{url('payment/add?id='.$s->id)}}">
                           <i class="fa fa-plus"></i>
                        </a>
                        
                         <a class="btn btn-primary btn-sm" title="Edit Customer" href="{{url('customer/edit?id='.$s->id)}}">
                           <i class="fa fa-edit"></i>
                        </a>
                        
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
    <p>
        {{ $customers->appends(Input::except('page'))->links() }}
    </p>
</div>