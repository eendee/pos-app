@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')
<?php 
    $url = 'admin/student/upload';
    $method='post';

?>

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Customers <span class=""> <a href="{{url('customer/edit?id=0')}}" class="btn btn-success"> add new </a> </span> </h2>
            
            <div class="row">
                <div class="col-sm-12">
                    @include('customer/list-partial')
                </div>
            </div>
        </div>
    </div>

@stop