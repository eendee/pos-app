<div class="">
    <div class="col-sm-6 pull-right">
        <form method="get" action="{{url('/payment/received/search')}}">
   
            <div class="input-group">
             {{Form::text('term',isset($term)?$term:'',array('class'=>'form-control',  'required'=>"required",'placeholder'=>'search by customer name, payment code or invoice code'))}}
              <span class="input-group-btn">
                {{ Form::submit('Search',array('class'=>'btn btn-primary pull-right'))}}
              </span>
            </div><!-- /input-group -->
        </form>
    </br>
    </div>
    <table class="table table-striped ">
        <tr>
        
            <th>Customer</th>
            <th>Payment Code</th>
            <th>Invoice Code</th>
            <th>Channel</th>
            <th>Amount</th>
            <th>Date</th>
            <th></th>
        </tr>

        <?php  $total=0;?>
        @if(isset($payments))
            @foreach($payments as $p)

                <tr>
                   
                    <td>
                        {{$p->fullname}}
                    </td>
                    <td>
                      {{$p->code}}
                    </td>    
                      <td>
                      {{$p->invoice_code}}
                    </td>  
                    <td>
                        {{$p->channel_name}}
                    </td>
                    <td>
                       {{$p->amount}}
                       <?php $total+= $p->amount; ?>
                    </td>
                   
                    <td>
                        {{$p->date_paid}}
                    </td>
                    <td>
                        <a class="btn btn-success btn-sm" target="_blank" title="Print Receipt" href="{{url('payment/receipt?print=true&code='.$p->code)}}">
                           <i class="fa  fa-print"></i>
                        </a>
                        <a class="btn btn-default btn-sm" title="Add new  invoice" href="{{url('payment/add?id='.$p->customer_id)}}">
                           <i class="fa fa-plus"></i>
                        </a>
                         <a class="btn btn-primary btn-sm" title="View Invoice" href="{{url('payment/invoice?code='.$p->invoice_code)}}">
                           <i class="fa fa-file"></i>
                        </a>
                        
                        <a class="btn btn-default btn-sm" target="_blank" title="Print Invoice" href="{{url('payment/invoice?print=true&code='.$p->invoice_code)}}">
                           <i class="fa fa-print"></i>
                        </a>
                        
                        @if($p->transaction_status!==2)
                            <a class="btn btn-success btn-sm" title="Add payments to invoice" href="{{url('payment/invoice/update?id='.$p->transaction_id)}}">
                                <i class="fa fa-money"></i>
                            </a>
                        @endif

                         <a class="btn btn-danger btn-sm" title="Delete Payment" href="{{url('payment/invoice/delete-payment?id='.$p->id)}}">
                           <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td> </td>
                <td> </td>    
                <td> </td>
                
                <td>
                    Total
                </td>
                <td>
                    {{number_format($total, 2, '.', ',')}}
                </td>
                <td></td>
                <td></td>
            </tr>
        @endif
    </table>
    <p>
        {{ $payments->appends(Input::except('page'))->links() }}
    </p>
</div>