@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

<div class="card">
<div class="card-body">

    <div class="row">
      

        <div class="col-md-5">
              <h2>Add payment to invoice</h2>
            {!! Form::model($invoice,array('url' => 'payment/invoice/update','method' => 'post', 'class'=>'form-horizontal')) !!}
        
            <table class="table">
                <tr>
                    <td>Payment type</td>
                   <td>{{Form::select('transaction_status',[''=>'select'] + $transaction_statuses,'',array('class'=>'form-control','required'=>'required'))}}</td>
                </tr>
                <tr>
                    <td>Payment Channel</td>
                    <td>{{Form::select('payment_channel_id',[''=>'select']+ $payment_channles,'',array('class'=>'form-control','required'=>'required'))}}</td>
                </tr>
                <tr>
                    <td>Transaction Date  </td>
                    <td>{{Form::text('date_paid',date("Y-m-d"),array('class'=>'form-control datepicker' , 'required'=>'required'))}}</td>
                </tr>
                <tr>
                    <td>Amount </td>
                    <td>{{Form::number('amount',$invoice->GetBalance(),array('class'=>'form-control','required'=>'required'))}}</td>
                </tr>
                
                <tr>
                    <input type="hidden" name="transaction_id" value="{{$invoice->id}}">
                    <td>Processed as </td>
                    <td>{{Auth::user()->Staff->Fullname()}}</td>
                </tr>
              
                <tr>
                    <td></td>
                    <td>
                    {{ Form::submit('Update',array('class'=>'btn btn-success pull-right'))}}
                    </td>
                </tr>
            </table>

            {{ Form::token() }}
            {!! Form::close() !!}
        </div>
        <div class="" style="font-size:.7em; height:400px; overflow-y: scroll; overflow-x: hidden;">
            @include('invoice/invoice')
        </div>
    </div>
</div>
</div>

@stop