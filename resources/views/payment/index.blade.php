@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')
<?php 
    $url = 'admin/student/upload';
    $method='post';

?>

@section('page')

    <div class="card">
        <div class="card-body">
           
            <div class="col-sm-5">
             <h2>Payments <?php echo isset($_GET['date'])? ' collected on '.$_GET['date']:'' ?>  </h2>
                <input type="text" id="date-select" class="form-control datepicker"  value="<?php echo isset($_GET['date'])?$_GET['date']:date('Y-m-d')  ?>">
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @include('payment/list-partial')
                </div>
            </div>
        </div>
    </div>
@stop