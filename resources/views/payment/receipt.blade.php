
<div class="col-sm-12">
    
    <section class="invoice" style="padding-top:1px !important;">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                
                <h2 class="page-header"> <img style="width:50px" src="{{url($details->logo)}}" alt="..." class="img-responsive"> Receipt # <small>{{$payment->code}}</small></h2>
            </div>
            
        </div>
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                @include('includes/company-details')
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>{{$invoice->fullname}}</strong><br>
                    {{$invoice->Customer->address}}<br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
            
                <b>Our Ref: </b> {{$payment->code}}<br>                
                <b> Date Generated</b> {{$invoice->created_at->toDateString()}}<br>
                <b>Due Date</b> {{$invoice->due_date}}<br>
                 {!! QrCode::size(240)->generate($payment->code); !!}<br>
                   </div>
            <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
            <div class="col-xs-12 table-responsive">
                <h4>Reference Invoice</h4>
                Invoice Number: {{$invoice->code}} 
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Item</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Sub total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($invoice->Items))
                            <?php $count=1 ?>
                            @foreach ($invoice->Items as $item)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>&#8358;{{$item->amount}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>&#8358;{{floatval($item->amount) * floatval($item->quantity)}}</td>
                                </tr>
                                <?php $count++ ?>
                            @endforeach
                                                        
                        @endif
                  
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Sum</td>
                            <td>&#8358;{{$invoice->amount}}</td>
                    
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Discount</td>
                            <td>&#8358;{{$invoice->discount}}</td>
                        </tr>
                        @if($invoice->vat_included)
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>VAT ({{$invoice->vat_percentage}}%)</td>
                                <td>&#8358;{{$invoice->CalculateVAT()}}</td>
                            </tr>
                        @endif
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th>Total</th>
                            <th>&#8358;{{$invoice->total}}</th>
                        </tr> 
                    </tbody>                
                </table>
    </br>
            <h4>Payment Details</h4>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td></td>
                            <td>Amount Paid</td>
                            <th>&#8358;{{$payment->amount}}</th>
                            <td>Date</td>
                            <th>{{$payment->date_paid}}</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Payment Code</td>
                            <th>{{$payment->code}}</th>
                            <td>Payment Channel</td>
                            <th>{{$payment->Channel->name}}</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Received By</td>
                            <th>{{$payment->Initiator->Staff->fullname()}}</th>
                            <td></td>
                            <td></td>
                        </tr>

                      </tbody>                
                </table>
            </div>
    
            <!-- /.col -->
        </div>
        <!-- /.row -->


      <!-- /.row -->

      <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="{{url('payment/receipt?code='.$payment->code.'&print=true')}}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                </button>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>