@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

<div class="card">
<div class="card-body">

    <div class="row">
      

        <div class="col-sm-8">
              <h2>Delete payment for  Invoice number {{$invoice->code}}</h2>
              <p>
                Payment Details:   <br>
                Amount: {{$payment->amount}} <br>
                Channel: {{$payment->Channel->name}} <br>
                Date: {{$payment->created_at->toDateString()}}

              </p>
              <div class="alert alert-warning">
                Kinldy note that even though this payment is deleted, a copy of the transaction is kept in the system for audit purposes
              </div>
            {!! Form::model('',array('url' => 'payment/invoice/delete-payment','method' => 'post', 'class'=>'form-horizontal')) !!}
        
            <table class="table">
  
                <tr>
                    <td>Reason for deleting the payment </td>
                    <td>{{Form::textarea('reason','',array('class'=>'form-control', 'rows'=>'2','required'=>'required'))}}</td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                    <input type="hidden" name="id" value="{{$payment->id}}">
                    {{ Form::submit('Delete Payment',array('class'=>'btn btn-danger pull-right'))}}
                    </td>
                </tr>
            </table>

            {{ Form::token() }}
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>

@stop