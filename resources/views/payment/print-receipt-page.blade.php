
@extends('layouts/print-container')

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Payment Receipt</h2>            
            <div class="row">
                @include('payment/receipt')
            </div>
        </div>
    </div>

@stop