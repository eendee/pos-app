@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Payment Receipt</h2>            
            <div class="row">
               <div class="col-sm-7" style="height:400px; overflow-y: scroll; overflow-x: hidden;">
                    @include('payment/receipt')
               </div>
            </div>
        </div>
    </div>

@stop