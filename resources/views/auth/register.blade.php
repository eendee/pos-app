@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">
                     <h3 class="mb-0">Create Account</h3>
                       </div>
                

                <div class="card-body">
                <p class="alert alert-success"> Please use the same email address supplied when creating your staff profile. </p>
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    <form method="POST" action="{{url('account/activate')}}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="">{{ __('E-Mail Address') }}</label>

                           
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div>


                        <div class="form-group">
                            <label for="password" class="">{{ __('Password') }}</label>

                          
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        
                        </div>
                         <div class="form-group">
                            <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>

                          
                                <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">

                                

                        
                        </div>


                        <div class="form-group">
                            <div class="float-right">
                                <button type="submit" class="btn btn-lg btn-success">
                                    Create Account
                                </button>
                            </div>
                            <div class="float-left">
                              
                                <p><a class="btn btn-link " href="{{ route('login') }}">
                                        Login
                                    </a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
