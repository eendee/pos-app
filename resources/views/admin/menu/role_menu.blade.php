@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')
@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Role Menus for {{$role->name}}</h2>
            <table class="table table-stripped table-bordered">
                <tr>
                    <th>Name</th>
                    <th></th>
                </tr>
                @if(isset($menus))
                {!! Form::model(null,array('url' => 'admin/roles/menus/update','method' => 'post', 'class'=>'form-horizontal')) !!}
                    @foreach($menus as $m)
                        <tr>
                            <td>{{$m->name}}</td>
                            <?php 
                                $checked=false;
                                if(in_array($m->id, $existing)){
                                    $checked=true;
                                }
                            ?>
                            <td>{{Form::checkbox("menu[$m->id]",$m->id,$checked)}}</td>  
                        </tr>
                    @endforeach
                <tr>
                    <td></td>
                    <td>
                        {{Form::hidden('role_id',$role->id)}}
                        {{ Form::submit('Save',array('class'=>'btn btn-success'))}}
                    </td>
                </tr>
                @endif
            </table>
        </div>
    </div>

@stop