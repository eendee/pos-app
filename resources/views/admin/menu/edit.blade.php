@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

<?php 
	$url = 'admin/menu/edit';
	$method='POST';
	if(isset($menu_)&&$menu_->id>=1){
		$url="admin/menu/edit?id=$menu_->id";
		$method="POST";
	}
?>

@section('page')

<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <h2>Add/Edit Menu</h2>

            {!! Form::model($menu_,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
            <div class="col-sm-8">
                <table class="table">
                    <tr>
                        <td>Name</td>
                        <td>{{Form::text('name',null,array('class'=>'form-control'))}}</td>
                    </tr>
                    <tr>
                        <td>Url</td>
                        <td>{{Form::text('url',null,array('class'=>'form-control'))}}</td>
                    </tr>
                    <tr>
                        <td>Menu Category</td>
                        <td>{{Form::select('menu_category_id', array(''=>'select')+$mainMenus,$menu_->menu_category_id,array('class'=>'form-control'))}}</td>
                    </tr>
                    <tr>
                        <td>Priority</td>
                        <td>{{Form::select('priority', $numberList1_10,$menu_->priority,array('class'=>'form-control'))}}</td>
                    </tr>
                    <tr>
                        <td>
                            Displayed
                        </td>
                        <td>
                            {{Form::hidden('displayed','0')}}
                            {{Form::checkbox('displayed')}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Activated
                        </td>
                        <td>
                            {{Form::hidden('activated','0')}}
                            {{Form::checkbox('activated')}}
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                        {{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
                        </td>
                    </tr>
                </table>
            </div>
            {{ Form::token() }}
            {!! Form::close() !!}
        </div>
    </div>
</div>  
@stop