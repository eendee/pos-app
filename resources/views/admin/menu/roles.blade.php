@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')
@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Roles</h2>
            <table class="table table-stripped table-bordered">
                    <tr>
                        <th>Name</th>
                        <th></th>
                    </tr>
                    @if(isset($roles))
                        @foreach($roles as $r)
                            <tr>
                                <td>{{$r->name}}</td>
                                <td><a href="{{url('admin/roles/menus?id='.$r->id)}}">Menus</a></td>	
                                        
                            </div>
                            </tr>
                        @endforeach
                @endif
            </table>
        </div>
    </div>

@stop