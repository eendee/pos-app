@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

    <div class="card">
        <div class="card-body">
            <section>
                <h2>Menus</h2>
                <p><a href="{{url('admin/menu/edit')}}" class="btn btn-primary pull-right"> Add new</a></p>
            </section>
            <table class="table table-stripped table-bordered">
                    <tr>
                        <th>Name</th>
                        <th>Parent</th>
                        <th>Url</th>
                        <th>Displayed</th>
                        <th></th>
                    </tr>
                    @if(isset($menus))
                        @foreach($menus as $m)
                            <tr>
                                <td>{{$m->name}}</td>
                                <td>{{$m->MenuCategory->name}}</td>
                                <td>{{$m->url}}</td>
                                <td>{{$m->displayed=='1'?'Yes':'No'}}</td>
                                <td><a href="{{url('admin/menu/edit?id='.$m->id)}}">Edit</a></td>	
                                        
                            </div>
                            </tr>
                        @endforeach
                @endif
            </table>
        </div>
    </div>

@stop