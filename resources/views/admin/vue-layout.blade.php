@extends('layouts.main')
@section('content')
<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">{{$meta->title}}</h2>
  </div>
</header>
<div id="app">
  <div class="breadcrumb-holder container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">{{$meta->active}}</a></li>
      <li class="breadcrumb-item active">{{$meta->title}}</li>
    </ul>
  </div>
  <section  class="tables">
    <div class="container-fluid">
      {!!$component!!}
    </div>
  </section>
</div>
<script src="{{asset('js/app.js')}}"></script>
@endsection