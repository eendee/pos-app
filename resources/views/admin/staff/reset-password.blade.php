@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

    <div class="col-sm-12">
        <div class="card">
             <div class="card-body">
            <h1 class="">Reset Staff Password </h1>
            <hr class="colorgraph">
                <div class="col-md-6">
                    @if(Session::has('message'))
                        <div class="alert alert-danger">
                            {{Session::get('message')}}
                        </div>
                    @endif
                    {!! Form::model(null,array('url' => 'admin/staff/reset-password','method' => 'post', 'class'=>'form-horizontal')) !!}
                
                        <div class="form-group">
                            {{Form::label('Staff Email')}}
                            {{Form::text('email',null,array('class'=>'form-control','required'=>'required'))}}
                        </div>
                        <div class="form-group">
                            {{Form::label('New Password')}}
                            {{Form::password('password',array('class'=>'form-control', 'required'=>'required'))}}
                        </div>
                        <div class="form-group">
                            {{Form::label('Retype New Password')}}
                            {{Form::password('password_confirmation',array('class'=>'form-control', 'required'=>'required'))}}
                        </div>
            
                        <div>
                            {{ Form::submit('Save',array('class'=>'btn btn-danger btn-dark pull-right'))}}
                        </div>
                        {{ Form::token() }}
                    {!! Form::close() !!}
                </div>
        </div>
        </div>

    </div>


@stop