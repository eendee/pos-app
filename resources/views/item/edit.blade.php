@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

<?php 
	$url = 'item/edit';
	$method='POST';
	if(isset($item)&&$item->id>=1){
		$url="item/edit?id=$item->id";
		$method="POST";
	}
?>

@section('page')
<div class="card">
<div class="card-body">

    <div class="row">
      

        <div class="col-sm-8">
              <h2>Add Item </h2>
            {!! Form::model($item,array('url' => $url,'method' => $method, 'class'=>'form-horizontal')) !!}
        
            <table class="table">
             
           
                
                <tr>
                    <td>Product/Service Name</td>
                    <td>{{Form::text('name',null,array('class'=>'form-control' , 'required'=>'required'))}}</td>
                </tr>
                <tr>
                    <td>Amount </td>
                    <td>{{Form::number('amount',null,array('class'=>'form-control','min'=>1, 'step'=>0.1 ))}}</td>
                </tr>
              
                <tr>
                    <td>Activated </td>
                    <td>
                        {{Form::hidden('activated','0')}}
                        {{Form::checkbox('activated')}}
                    </td>
                </tr>
                

                <tr>
                    <td></td>
                    <td>
                        {{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
                    </td>
                </tr>
            </table>

            {{ Form::token() }}
            {!! Form::close() !!}
        </div>

    </div>
</div>
</div>
@stop