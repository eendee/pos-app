<div class="">
    <div class="col-sm-6 pull-right">
        <form method="get" action="{{url('/item/search')}}">
   
            <div class="input-group">
             {{Form::text('term',isset($term)?$term:'',array('class'=>'form-control',  'required'=>"required",'placeholder'=>'search by name'))}}
              <span class="input-group-btn">
                {{ Form::submit('Search',array('class'=>'btn btn-primary pull-right'))}}
              </span>
            </div><!-- /input-group -->
        </form>
    </br>
    </div>
    <table class="table table-striped ">
        <tr>
            <th>Name</th>
            <th>Amount</th>
            <th>Active</th>
            <th></th>
        </tr>

        <?php  $count=0;?>
        @if(isset($items))
            @foreach($items as $i)

                <?php  $count++;?>
                <tr>
                   
                    <td>
                      {{$i->name}}
                    </td>    
                    <td>
                      {{$i->amount}}
                    </td>  
                    <td>
                        @if($i->activated ==1)
                            <span>True</span>
                        @else
                            <span>False</span>
                        @endif
                    </td>  
                    <td>
                        
                        <form method="post" action="{{url('item/delete')}}" onsubmit="return confirm('Are you sure you want to delete this item?');">
                            <a class="btn btn-primary btn-sm" title="Edit item" href="{{url('item/edit?id='.$i->id)}}">
                               Edit
                            </a>
                            {{ Form::token() }}
                                <input type="hidden" name="id" value="{{$i->id}}">
                                <input type="submit" value="delete" class="btn btn-sm btn-danger">
                            {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
    <p>
        {{ $items->appends(Input::except('page'))->links() }}
    </p>
</div>