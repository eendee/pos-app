<!DOCTYPE html>
<html>
@include('layouts/head-tag')
  <body>
    <div class="page" data-app="true">
      <!-- Main Navbar-->
      @include('layouts/header')
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        
        @include('layouts.sidebar')
        
        <div class="content-inner">
          <!-- Page Header-->
        
        @yield('content')
        
          <!-- Page Footer-->
        @include('layouts/footer')
        </div>
      </div>
    </div>
    <!-- JavaScript files-->

    @include('layouts.tail-tags')
  </body>
</html>