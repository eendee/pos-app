          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-6">
                  <p> &copy;<?php echo Date('Y') ?></p>
                </div>
                <div class="col-sm-6 text-right">
                  <p> by <a href="#" class="external">Velexo</a></p>
                </div>
              </div>
            </div>
          </footer>