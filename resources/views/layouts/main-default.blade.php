@extends('layouts.main')
@section('content')
<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">@yield('title')</h2>
  </div>
</header>
<div id="app">
  <div class="breadcrumb-holder container-fluid">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">@yield('active')</a></li>
      <li class="breadcrumb-item active">@yield('title')</li>
    </ul>
  </div>
  <section  class="tables">
    <div class="container-fluid">
        @if(Session::has('message'))
          <div class="alert bg-green has-shadow alert-info">
              {{
                Session::get('message')
              }}
          </div>
        @endif
        @if(Session::has('error'))
          <div class="alert bg-red has-shadow alert-danger">
              {{
                Session::get('error')
              }}
          </div>
        @endif
        @if(isset($errors))
            @if($errors->count())
                <div class="alert bg-red has-shadow alert-danger">
                  @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                  @endforeach
                </div>
            @endif
        @endif
       @yield('page')
    </div>
  </section>
</div>
@stop