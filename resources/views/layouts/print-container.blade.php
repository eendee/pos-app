<!DOCTYPE html>
<html>
@include('layouts/head-tag')
  <body>
    <div class="page" data-app="true">
      <div class="page-content d-flex align-items-stretch"> 
        
        <div class="content-inner">
          <!-- Page Header-->
        
        @yield('page')
        
        </div>
      </div>
    </div>
    <!-- JavaScript files-->

    @include('layouts.tail-tags')
  </body>
    <script type="text/javascript">
        window.onload = function() { window.print(); }
    </script>
</html>