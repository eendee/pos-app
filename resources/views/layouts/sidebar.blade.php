<?php
use App\Helpers\SiteHelper;  
?>
        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <?php $img_path=Auth::user()->GetProfilePicture(); ?>
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="{{url($img_path)}}" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">{{SiteHelper::ShortenString(Auth::user()->name,15)}}</h1>
              <p>{{Auth::user()->Role->name}}</p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
            <li class="{{(isset($menu) && isset($menu['Home'])) ? 'active' : ' ' }}">
              <a href="{{url('/')}}"> <i class="icon-home"></i>Home </a>
            </li>
            @if(isset($menuItems))        
                @foreach($menuItems as $key=>$val)
                    <?php
                        //dd($menu);
                        $slug=str_replace(" ","",$key);
                        $active=(isset(($menu)) && isset($menu[$key]) && $menu[$key]!="")?$active="active":"";    
                    ?>
                    <li class="{{$active}}">
                        <a href="#{{$slug}}" aria-expanded="{{$active==='active'?'true':'false'}}" data-toggle="collapse" >
                          <i class="fa fa-pie-chart"></i> {{$key}}
                        </a>

                        <ul class="collapse list-unstyled  collapse {{$active==='active'?'show':''}}" id="{{$slug}}">
                          @foreach($val as $m)
                              <li><a href="{{url($m['url'])}}"><i class="fa fa-circle-o"></i> {{$m['name']}}</a></li>
                          @endforeach
                        </ul>
                    </li>
                    
                    
                @endforeach
            @endif
          </ul>            
        </nav>