

    
<script>

     $(function () {

        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

        $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            setDate: today,
            todayHighlight:true
        });

        $('#date-select').on('change',function(val){
            window.location.replace(window.location.toString().replace(location.search, "")+ "?date="+$('#date-select').val());
        })
     })

     
</script>