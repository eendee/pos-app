@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Settings  </h2>
            
            <div class="row">
                <div class="col-sm-12">
                 {!! Form::model(null,array('url' => 'settings','method' => 'post', 'class'=>'form-horizontal')) !!}
                       
                   <table class="table">
                        <tr>
                            <th>
                                Setting
                            </th>
                            <th>
                                Value
                            </th>
                        </tr>
                        @if(isset($settings))
                            <tr>

                                <td>Organization Name </td>
                                {{Form::hidden('setting[2][id]',$settings[2]->id)}}
                                <td>{{Form::text('setting[2][value]',$settings[2]->value,array('class'=>'form-control' ))}}</td>
                            </tr>
                             <tr>
                                <td>Organization Address </td>
                                {{Form::hidden('setting[3][id]',$settings[3]->id)}}
                                <td>{{Form::textarea('setting[3][value]',$settings[3]->value,array('class'=>'form-control', 'rows'=>'2' ))}}</td>
                            </tr>
                            <tr>
                                <td>Organization phone </td>
                                {{Form::hidden('setting[6][id]',$settings[6]->id)}}
                                <td>{{Form::text('setting[6][value]',$settings[6]->value,array('class'=>'form-control' ))}}</td>
                            </tr>
                            <tr>
                                <td>Organization email </td>
                                {{Form::hidden('setting[7][id]',$settings[7]->id)}}
                                <td>{{Form::text('setting[7][value]',$settings[7]->value,array('class'=>'form-control' ))}}</td>
                            </tr>
                            <tr>
                                <td>Organization website </td>
                                {{Form::hidden('setting[8][id]',$settings[8]->id)}}
                                <td>{{Form::text('setting[8][value]',$settings[8]->value,array('class'=>'form-control' ))}}</td>
                            </tr>
                            <tr>
                                <td>Bank Details </td>
                                {{Form::hidden('setting[5][id]',$settings[5]->id)}}
                                <td>{{Form::textarea('setting[5][value]',$settings[5]->value,array('class'=>'form-control', 'rows'=>'2' ))}}</td>
                            </tr>
                            <tr>
                                    
                                <td>
                                    {{Form::hidden('setting[0][id]',$settings[0]->id)}}
                                    {{$settings[0]->public_name}}
                                </td>    
                                 <td>
                                    {{Form::hidden('setting[0][value]','0')}}
                                    <input name="setting[0][value]" type="checkbox" value="1" {{($settings[0]->value==1)?'checked="checked"':''}}>
                                </td>
                                
                            </tr>
                            <tr>                                    
                                <td>
                                    {{Form::hidden('setting[1][id]',$settings[1]->id)}}
                                    {{$settings[1]->public_name}}
                                </td>    
                                 <td>
                                    {{Form::number('setting[1][value]',$settings[1]->value,array('class'=>'form-control','min'=>0, 'step'=>0.1, 'max'=>100, 'style'=>'width:100px' ))}}
                                </td>
                                
                            </tr>

                           
                            <tr>
                                    
                                <td>
                                 </td> 
                          
                                <td>
                                    {{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
                                </td>  
                                
                            </tr>

                        @endif
                   </table>
                    {{ Form::token() }}
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop