@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')
<div class="card">
	<div class="card-body">
		<h1 class="">Update  Company Logo</h1>
		<hr class="colorgraph">
        <div class="row">
            <div class="col-md-6">
                {!! Form::model(null,array('url' => 'settings/logo','method' => 'post','enctype'=>'multipart/form-data', 'class'=>'form-horizontal')) !!}

                    <div class="form-group">
                        {{Form::label('Company Logo')}}
                        {{Form::file('file')}}
                    </div>
                    <div>
                        {{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
                    </div>
                    {{ Form::token() }}
                {!! Form::close() !!}
            </div>
            <div class="col-md-6">
                <div style="background:#f2f2f2;">
                    <h5>Current Logo</h5>
                    <img class="img-responsive" style="max-width:400px;" src="{{url($logo)}}" />
                </div>
            </div>
        </div>
	</div>
</div>

@endsection
