<address>
    <strong>{{$details->name}}</strong><br>
    {{$details->address}}<br>
    
    <div style="font-size:.8em;">
        <i class="fa fa-phone"></i> {{$details->phone}}<br>
        <i class="fa fa-envelope"></i> {{$details->email}}<br>
        <i class="fa fa-globe "></i> {{$details->website}}
    </div>

    <span class="text-muted">{{date("Y-m-d")}}</span>
</address>