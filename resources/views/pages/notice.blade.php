@section('title', 'Something went wrong')
@section('active', $meta->active)
@extends('layouts/main-default')
@section('page')

<div class ="card">

    <div class ="card-body">
        <div style="max-width:95%;">
            <div class=" {{isset($messageType)?'alert alert-'.$messageType:'alert alert-danger'}}">
                <h4>{{$message}}</h4>
            </div>
        </div>
    </div>
</div>
@stop