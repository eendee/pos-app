@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')
<?php 
    $url = 'admin/student/upload';
    $method='post';

?>

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Invoices  of {{$customer->fullname()}}<span class=""> </h2>
            <p><a href="{{url('payment/add?id='.$customer->id)}}" class="btn btn-default "> add new </a> </span> </p>
            <div class="row">
                <div class="col-sm-12">
                    @include('invoice/list-partial')
                </div>
            </div>
        </div>
    </div>

@stop