<div class="">
    <div class="col-sm-6 pull-right">
        <form method="get" action="{{url('/payment/invoices/search')}}">
   
            <div class="input-group">
             {{Form::text('term',isset($term)?$term:'',array('class'=>'form-control',  'required'=>"required",'placeholder'=>'search by name or code'))}}
              <span class="input-group-btn">
                {{ Form::submit('Search',array('class'=>'btn btn-primary pull-right'))}}
              </span>
            </div><!-- /input-group -->
        </form>
    </br>
    </div>
    <table class="table table-striped ">
        <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Due Date</th>
            <th></th>
        </tr>

        <?php  $count=0;?>
        @if(isset($invoices))
            @foreach($invoices as $p)

                <?php  $count++;?>
                <tr>
                   
                    <td>
                      {{$p->fullname}}
                    </td>    
                      <td>
                      {{$p->code}}
                    </td>  
                    <td>
                        {{$p->total}}
                    </td>
                    <td>
                        <span class="badge badge-{{$p->GetTransactionBadge()}}">{{$p->Status->name}}</span> 
                    </td>
                    <td>
                        {{$p->due_date}}
                    </td>
                    <td>
                        <a class="btn btn-default btn-sm" title="Add Customer Invoice" href="{{url('payment/add?id='.$p->customer_id)}}">
                           <i class="fa fa-plus"></i>
                        </a>
                         <a class="btn btn-primary btn-sm" title="View Invoice" href="{{url('payment/invoice?code='.$p->code)}}">
                           <i class="fa fa-file"></i>
                        </a>
                        <a class="btn btn-primary btn-sm" target="_blank" title="Print Invoice" href="{{url('payment/invoice?print=true&code='.$p->code)}}">
                           <i class="fa fa-print"></i>
                        </a>
                        
                        @if($p->transaction_status!==2)
                            <a class="btn btn-success btn-sm" title="Invoice Payments" href="{{url('payment/invoice/update?id='.$p->id)}}">
                                <i class="fa fa-money"></i>
                            </a>
                        @endif

                         <a class="btn btn-danger btn-sm" title="Remove Invoice" href="{{url('payment/invoice/delete?id='.$p->code)}}">
                           <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
    <p>
        {{ $invoices->appends(Input::except('page'))->links() }}
    </p>
</div>