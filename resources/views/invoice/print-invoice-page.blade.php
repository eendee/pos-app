
@extends('layouts/print-container')

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Invoice</h2>            
            <div class="row">
                @include('invoice/invoice')
            </div>
        </div>
    </div>

@stop