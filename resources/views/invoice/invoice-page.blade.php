@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Invoice</h2>            
            <div class="row">
               <div class="col-sm-5">
                    <div class="bd-example">
                        <h2>Payments:</h2>
                        @include('invoice/payment-list-partial')
                    </div>
                    <div>
                        @if($invoice->GetBalance()>0)
                            <a class="btn btn-success" href="{{url('payment/invoice/update?id='.$invoice->id)}}">Add Payment </a>
                        @endif
                        <a class="btn btn-danger" href="{{url('payment/invoice/delete?id='.$invoice->code)}}">Delete this Invoice </a>
                        
                    </div>
               </div>
               <div class="col-sm-7" style="height:400px; overflow-y: scroll; overflow-x: hidden;">
                    @include('invoice/invoice')
               </div>
            </div>
        </div>
    </div>

@stop