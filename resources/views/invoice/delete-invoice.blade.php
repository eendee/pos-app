@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

<div class="card">
<div class="card-body">

    <div class="row">
      

        <div class="col-sm-8">
              <h2>
                Delete  Invoice  {{$invoice->code}} generated for {{$invoice->fullname}}
              </h2>
              <div class="row">
                <div class="col-sm-6">
                <table class="table">
                  <tr>
                    <td>Amount</td>
                    <td>{{$invoice->total}} </td>
                  <tr>
                  <tr>
                    <td>Date generated</td>
                    <td>{{$invoice->created_at->toDateString()}}</td>
                  <tr>
                  <tr>
                    <td>Status</td>
                    <td><span class="badge badge-{{$invoice->GetTransactionBadge()}}">{{$invoice->Status->name}}</span> </td>
                  <tr>
                </table>
               
              </div>
              <div class="col-sm-6 alert alert-warning">
                @include('invoice/payment-list-partial',array('hide'=>true))
              </div>
              </div>
              
              
                <span style="color:red"> * This will also delete any payments associated with the invoice</span>
              <div class="alert alert-warning">
                Kinldy note that even though this invoice is deleted, a copy  is kept in the system for audit purposes
              </div>
            {!! Form::model('',array('url' => 'payment/invoice/delete','method' => 'post', 'class'=>'form-horizontal')) !!}
        
            <table class="table">
  
                <tr>
                    <td>Reason for deleting the invoice </td>
                    <td>{{Form::textarea('reason','',array('class'=>'form-control', 'rows'=>'2','required'=>'required'))}}</td>
                </tr>

                <tr>
                    <td></td>
                    <td>
                    <input type="hidden" name="id" value="{{$invoice->id}}">
                    {{ Form::submit('Delete Invoice',array('class'=>'btn btn-danger pull-right'))}}
                    </td>
                </tr>
            </table>

            {{ Form::token() }}
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>

@stop