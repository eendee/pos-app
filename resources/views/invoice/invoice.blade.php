
<div class="col-sm-12">
    
    <section class="invoice" style="padding-top:1px !important;">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                
                <h2 class="page-header"> <img style="width:50px" src="{{url($details->logo)}}" alt="..." class="img-responsive"> Invoice <small>{{$invoice->code}}</small></h2>
            </div>
        </div>
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                @include('includes/company-details')
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>{{$invoice->fullname}}</strong><br>
                    {{$invoice->Customer->address}}<br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice Code: </b> {{$invoice->code}}<br>                
                <b> Date Generated</b> {{$invoice->created_at->toDateString()}}<br>
                <b>Due Date</b> {{$invoice->due_date}}<br>
                <b> Status </b><span class="badge badge-{{$invoice->GetTransactionBadge()}}">{{$invoice->Status->name}}</span>  <br>
            </div>
            <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
            <div class="col-xs-12 table-responsive">
                @include('invoice/invoice-list-details-partial')
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-sm-8">
            <p class="lead">Payment Method: Bank Transfer</p>
          
            Bank Details :
            <span>{{$details->bank}}</span><br>
          {!! QrCode::size(240)->generate($invoice->code); !!}<br>
        </div>
        <!-- /.col -->
        <div class="col-sm-4">
            <p class="lead">Amount</p>

            <div class="">
                <table class="table table-striped table-condensed" style="font-size:.8em;">
                    <tr>
                        <td>Sum:</td>
                        <td>&#8358;{{$invoice->amount}}</td>
                    </tr>
                    <tr>
                        <td>Discount</td>
                        <td>&#8358;{{$invoice->discount}}</td>
                    </tr>
                    @if($invoice->vat_included)
                        <tr>
                            <td>VAT ({{$invoice->vat_percentage}}%)</td>
                            <td>&#8358;{{$invoice->CalculateVAT()}}</td>
                        </tr>
                    @endif
                    <tr>
                        <th>Total</th>
                        <th>&#8358;{{$invoice->total}}</th>
                    </tr>
                    
                    <?php $payments_total=0?>
                    <tr>
                        <td>Payments:</td>
                    </tr>
                    @if($invoice->Payments && count($invoice->Payments))
                        @foreach ($invoice->Payments as $p)
                            <?php $payments_total+=$p->amount;?>
                            <tr>
                                <td>{{$p->Channel->name}} : <span class="text-muted"> {{$p->created_at->toDateString()}} </span>   </td>
                                <td>&#8358;{{$p->amount}}</td>
                            </tr>
                        @endforeach   
                    @else
                         <tr>
                            <td>None</td>
                        </tr>                     
                    @endif

                    <tr>
                        <th>Balance Due</th>
                        <th><b>&#8358;{{ $invoice->GetBalance()}}</b></th>
                    </tr>
                   
                </table>
            </div>
        </div>
        <!-- /.col -->
    </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="{{url('payment/invoice?code='.$invoice->code.'&print=true')}}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                </button>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>