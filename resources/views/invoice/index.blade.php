@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')
<?php 
    $url = 'admin/student/upload';
    $method='post';

?>

@section('page')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                
                    <h2>Invoices <?php echo isset($_GET['date'])? ' created on '.$_GET['date']:'' ?> </h2>
                    <input type="text" id="date-select" class="form-control datepicker"  value="<?php echo isset($_GET['date'])?$_GET['date']:date('Y-m-d')  ?>">
                </div>
                <div class="col-sm-6">
                    <p><a href="{{url('payment/add?id=0')}}" class="btn btn-success pull-right "> add new </a> </span> </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @include('invoice/list-partial')
                </div>
            </div>
        </div>
    </div>

@stop