    <table class="table table-striped">
            <tr>
            
                <th>Channel</th>
                <th>Amount</th>
                <th>Date</th>
                <td></td>

            </tr>
            @if($invoice->Payments && count($invoice->Payments))
                @foreach ($invoice->Payments as $p)
                    
                    <tr>
                        <td>{{$p->Channel->name}} </td>
                        <td>{{$p->amount}}</td>
                        <td><span class="text-muted"> {{$p->created_at->toDateString()}} </span></td>
                        @if(!isset($hide))
                            <td>
                                <a class="btn btn-success btn-sm" href="{{url('payment/receipt?code='.$p->code)}}">
                                    <i class="fa fa-print"></i>
                                </a>
                                |
                                <a class="btn btn-danger btn-sm" href="{{url('payment/invoice/delete-payment?id='.$p->id)}}">
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        @endif
                    </tr>
                @endforeach   
            @else
                <tr>
                    <td>None</td>
                </tr>                     
            @endif

                        </table>