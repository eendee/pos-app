@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

    <div class="card">
        <div class="card-body">
            <h2>Invoices</h2>
            
            <div class="row">
                <div class="col-sm-12">
                    @if(isset($term))
                        <p>
                           Search results for <b>{{$term}}</b>
                        </p>
                    @endif
                    @include('invoice/list-partial')
                </div>
            </div>
        </div>
    </div>

@stop