<table class="table table-striped">
    <thead>
        <tr>
            <th>SN</th>
            <th>Item</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Sub total</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($invoice->Items))
            <?php $count=1 ?>
            @foreach ($invoice->Items as $item)
                <tr>
                    <td>{{$count}}</td>
                    <td>{{$item->name}}</td>
                    <td>&#8358;{{$item->amount}}</td>
                    <td>{{$item->quantity}}</td>
                    <td>&#8358;{{floatval($item->amount) * floatval($item->quantity)}}</td>
                </tr>
                <?php $count++ ?>
            @endforeach
                                        
        @endif
            <tr>
            <th></th>
            <th></th>
            <th></th>
            <td>Items Total</td>
            <td>&#8358;{{$invoice->amount}}</td>
        </tr>
    </tbody>
</table>