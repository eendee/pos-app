@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <hr class="colorgraph">
                <div class="col-md-6">
                    <div class="alert alert-success">
                        Password successfully changed.
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@stop