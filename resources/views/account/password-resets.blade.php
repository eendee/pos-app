@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')
<div class="card">
	<div class="card-body">
		<h1 class="">Log of password resets  </h1>
        <div class="alert alert-info"> 
            This shows the log of password reset requests that you have made. These requests are shown for transparency. 
        </div>
        <table class="table table-striped ">
            <tr>
                <th>SN</th>
                <th>Date</th>                
                <th>Carried out by</th>
            </tr>

            <?php  $count=0;?>
            @if(isset($logs))
                @foreach($logs as $l)

                    <?php  $count++;?>
                    <tr>                    
                        <td>
                            {{$count}}
                        </td>    
                        <td>
                            {{$l->created_at->toDateTimeString()}}
                        </td> 
                         
                        <td>
                            {{$l->Initiator->name}}
                        </td> 
                    </tr>
                @endforeach
            @endif
        </table>
		
	</div>
</div>

@endsection
