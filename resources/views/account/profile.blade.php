@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')
<div class="card">
	<div class="card-body">
		<h1 class="">Edit My Profile: <span class="text-muted">{{$staff->Fullname()}}</span> </h1>
		<hr class="colorgraph">
		<div class="col-md-6">
				@if(Session::has('message'))
					<div class="alert alert-danger">
						{{Session::get('message')}}
					</div>
				@endif
			{!! Form::model($staff,array('url' => 'account/profile','method' => 'post','enctype'=>'multipart/form-data', 'class'=>'form-horizontal')) !!}
		    	<div class="form-group">
					{{Form::label('Lastname')}}
					{{Form::text('lastname',$staff->lastname,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Firstname')}}
					{{Form::text('firstname',$staff->firstname,array('class'=>'form-control'))}}
				</div>

				<div class="form-group">
					{{Form::label('Middlename')}}
					{{Form::text('middlename',$staff->middlename,array('class'=>'form-control'))}}
				</div>
	
				<div class="form-group">
					{{Form::label('Profile Picture')}}
					{{Form::file('file')}}
				</div>
				<div class="form-group">
					{{Form::label('Address')}}
					{{Form::text('address',$staff->address,array('class'=>'form-control'))}}
				</div>
				<div class="form-group">
					{{Form::label('Phone')}}
					{{Form::text('phone',$staff->phone,array('class'=>'form-control'))}}
				</div>
				<div>
					{{ Form::submit('Save',array('class'=>'btn btn-success pull-right'))}}
				</div>
				{{ Form::token() }}
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection
