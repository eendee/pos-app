@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')

@section('page')

  <div class="project">
    <div class="row bg-white has-shadow">
      <div class="left-col col-lg-6 d-flex align-items-center justify-content-between">
        <div class="project-title d-flex align-items-center">
          <div class="image has-shadow"><img src="{{url($meta->company->logo)}}" alt="..." class="img-fluid"></div>
          <div class="text">
            <h3 class="h4">{{$meta->company->name}}</h3><small>{{$meta->company->address}}</small>
          </div>
        </div>
        <div class="project-date"><span class="hidden-sm-down">Today | {{date('Y-m-d')}}</span></div>
      </div>
      <div class="right-col col-lg-6 d-flex align-items-center">
        <div class="time"><i class="fa fa-clock-o"></i>{{date('h:i a')}} </div>
      
        <div class="project-progress">
     
        </div>
      </div>
    </div>
  </div>

         <section style="padding-top:10px;" class="dashboard-counts no-padding-bottom">
            <div class="">
              <div class="row bg-white has-shadow">
                <!-- Item -->
                <div class="col-xl-3 col-md-3">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-violet"><i class="icon-user"></i></div>
                    <div class="title"><span>Customer <br>Invoices</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
                      </div>
                    </div>
                    <div class="number"><strong>{{$meta->invoices}}</strong></div>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-3 col-md-3">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-red"><i class="icon-padnote"></i></div>
                    <div class="title"><span>Clients & <br>Customers</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 70%; height: 4px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
                      </div>
                    </div>
                    <div class="number"><strong>{{$meta->customers}}</strong></div>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-3 col-md-3">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-green"><i class="icon-bill"></i></div>
                    <div class="title"><span>Products & <br>Services</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 40%; height: 4px;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
                      </div>
                    </div>
                    <div class="number"><strong>{{$meta->services}}</strong></div>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-3 col-sm-3">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-orange"><i class="icon-check"></i></div>
                    <div class="title"><span>Payments<br>Processed</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
                      </div>
                    </div>
                    <div class="number"><strong>{{$meta->payments}}</strong></div>
                  </div>
                </div>
              </div>
            </div>
         </section
          


@stop