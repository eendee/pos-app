
@extends('pdf/pdf-layout')

@section('page')

    <div class="">
        <div class="row">
                <div class="col-md-12">
                    <div>
                    <h2>Invoice Reports</h2>
                        @if(isset($results) &&$results!=null)
                            <div style="font-size:.8em">
                                @if(isset($results)&& count($results))
                                    <table class="table table-striped table-bordered table-condensed">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Total</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($results as $i)
                                                <tr>
                                                    <td>
                                                        {{$i->code}}
                                                    </td>
                                                    <td>
                                                        {{$i->fullname}}
                                                    </td>
                                                    <td>
                                                        {{$i->date_paid}}
                                                    </td> 
                                                    <td>
                                                        {{$i->total}}
                                                    </td> 
                                                    <td>
                                                        {{$i->Status->name}}
                                                    </td> 
                                                </tr>
                                            @endforeach
                                        </tbody>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop