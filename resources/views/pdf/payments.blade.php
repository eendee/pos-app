
@extends('pdf/pdf-layout')

@section('page')

    <div class="">
        <div class="row">
                <div class="col-md-12">
                    <div>
                    <h2>Payment Reports</h2>
                    <?php $total = 0 ?>
                        @if(isset($results) &&$results!=null)
                            <div style="font-size:.8em">
                                @if(isset($results)&& count($results))
                                    <table class="table table-striped table-bordered table-condensed">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th>Payment Code</th>
                                                <th>Invoice Code</th>
                                                <th>Amount</th>
                                                <th>Channel</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($results as $p)
                                                <?php $total += $p->amount  ?>
                                                <tr>
                                                    <td>
                                                        {{$p->code}}
                                                    </td>
                                                    <td>
                                                        {{$p->invoice_code}}
                                                    </td>
                                                    <td>
                                                        {{$p->amount}}
                                                    </td> 
                                                    <td>
                                                        {{$p->channel_name}}
                                                    </td>
                                                    <td>
                                                        {{$p->created_at}}
                                                    </td>  
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <th></th>
                                                <th>Total</th> 
                                                <th>{{$total}}</th> 
                                                <th></th>
                                                <th></th>                                            
                                            </tr>
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop