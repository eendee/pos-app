@section('title', $meta->title)
@section('active', $meta->active)
@extends('layouts/main-default')
<?php 
    $url = 'admin/student/upload';
    $method='post';

?>

@section('page')

    <div class="card">
        <div class="card-body">
           
            <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-12">
                   <div class="">

                    <table class="table table-striped ">
                        <tr>
                        
                            <th>Customer</th>
                            <th>Reasoon for action</th>            
                            <th>Action by</th>
                            <th>Payment Code</th>
                            <th>Amount</th>
                            <th>Date of payment</th>
                            <th>Date of action</th>  
                        </tr>

                        @if(isset($payments))
                            @foreach($payments as $p)

                                <tr>
                                    <td>
                                        {{isset($p->Invoices[0]) && isset($p->Invoices[0]->Customer[0])? $p->Invoices[0]->Customer[0]->Fullname(): ''}}
                                    </td>
                                    
                                    <td>
                                        {{$p->reason}}
                                    </td> 
                                    
                                    <td>
                                        {{isset($p->Initiator)? $p->Initiator->name: ''}}
                                    </td>
                                    <td>
                                        {{$p->code}}
                                    </td>   
                                    <td>
                                        {{$p->amount}}
                                        </td>  
                                        <td>
                                            {{$p->date_paid}}
                                        </td>
                                        <td>
                                            {{$p->created_at}}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                    <p>
                        {{ $payments->appends(Input::except('page'))->links() }}
                    </p>
</div>
                </div>
            </div>
        </div>
    </div>
@stop