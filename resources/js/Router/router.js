import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import EditStaff from "../components/Staff/EditStaff";
import StaffList from "../components/Staff/StaffList";
import AddPaymentPage from "../components/Payment/AddPaymentPage";

const routes = [
    { path: "/admin/staff/edit-staff", component: EditStaff },
    { path: "/admin/staff/edit-staff/:id", component: EditStaff },
    { path: "/admin/staff", component: StaffList },
    { path: "/payment/add", component: AddPaymentPage },
    { path: "/payment/add/:id", component: AddPaymentPage }
];

const router = new VueRouter({
    routes, // short for `routes: routes`,
    hashbang: false,
    mode: "history"
});

export default router;
