<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Route::get('/', 'DashboardController@Dashboard');

#Account
Route::get('/account/activate', 'UserController@Activate')->name('activate');
Route::post('/account/activate', 'UserController@CreateAccount');
Route::get('/account/change-password', 'UserController@ChangePassword');
Route::post('/account/change-password', 'UserController@SavePassword');
Route::get('/account/dashboard', 'DashboardController@Dashboard');
Route::get('/account/password-resets', 'UserController@passwordResets');


Route::get('/account/profile', 'UserController@Profile');
Route::post('/account/profile', 'UserController@updateProfile');

Route::get('/payment/receipt', 'PaymentController@viewPaymentReceipt');


Route::get('/home', 'DashboardController@Dashboard')->name('home');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();
