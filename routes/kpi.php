<?php

Route::get('test', function () {
    dd("test");
});
Route::prefix('api2')->middleware(['auth'])->group(function () {

    Route::get('roles', 'ManageRoleController@index');

    Route::get('/staff', 'ManageStaffController@AllStaffJson');
    Route::get('/staff/{id}', 'ManageStaffController@OneStaffJson');
    Route::post('/staff/create', 'ManageStaffController@create');
    Route::post('/staff/update/{id}', 'ManageStaffController@update');
    Route::post('/staff/delete', 'ManageStaffController@delete');


    Route::get('items', 'ItemController@GetItemsJson');
    Route::get('settings', 'SettingController@GetSettingsJson');
    Route::get('user', 'UserController@GetUserJson');
    Route::get('customer', 'CustomerController@GetSingleCustomerJson');
    Route::get('payment-types', 'PaymentController@GetPaymentTypesJson');
    Route::get('transaction-statuses', 'PaymentController@GetTransactionTypesJson');


    Route::post('/transaction/save', 'PaymentController@save');
});
