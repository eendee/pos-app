<?php

Route::middleware(['auth', 'ProtectedPage'])->group(function () {

    #Customer
    Route::get('/customer', 'CustomerController@index');
    Route::get('/customer/edit', 'CustomerController@edit');
    Route::post('/customer/edit', 'CustomerController@save');
    Route::post('/customer/delete', 'CustomerController@delete');
    Route::get('/customer/search', 'CustomerController@search');

    #Payment
    Route::get('/payment', 'PaymentController@index');
    Route::get('/payment/invoices', 'PaymentController@index');
    Route::get('/payment/invoice', 'PaymentController@viewInvoice');
    Route::get('/payment/invoices/search', 'PaymentController@search');
    Route::get('payment/customer/invoices', 'PaymentController@searchByCustomer');
    Route::get('/payment/invoice/update', 'PaymentController@update');
    Route::post('/payment/invoice/update', 'PaymentController@updateStatus');

    Route::get('payment/invoice/delete-payment', 'PaymentController@deletePaymentPrompt');
    Route::post('payment/invoice/delete-payment', 'PaymentController@deletePayment');

    Route::get('payment/invoice/delete', 'PaymentController@deleteInvoicePrompt');
    Route::post('payment/invoice/delete', 'PaymentController@deleteInvoice');

    Route::get('/payment/received', 'PaymentController@payments');
    Route::get('/payment/received/search', 'PaymentController@SearchPayments');

    #Payment handled by vue router
    Route::get('/payment/add/{any}', 'PaymentController@add');
    Route::get('/payment/add', 'PaymentController@add');
    Route::get('/payment/{any}', 'PaymentController@index');

    #Item
    Route::get('/item', 'ItemController@index');
    Route::get('/item/edit', 'ItemController@edit');
    Route::post('/item/edit', 'ItemController@save');
    Route::post('/item/delete', 'ItemController@delete');
    Route::get('/item/search', 'ItemController@search');

    #Settings
    Route::get('/settings', 'SettingController@index');
    Route::post('/settings', 'SettingController@save');
    Route::get('/settings/logo', 'SettingController@editLogo');
    Route::post('/settings/logo', 'SettingController@saveLogo');






    #Menu
    Route::get('/admin/menu', 'MenuController@index');
    Route::get('/admin/roles/menus', 'MenuController@RoleMenus');
    Route::get('/admin/menu/edit', 'MenuController@edit');
    Route::post('/admin/menu/edit', 'MenuController@Save');

    #Roles
    Route::get('/admin/roles/', 'MenuController@roles');
    Route::post('/admin/roles/menus/update', 'MenuController@UpdateMenuInRole');



    #Reports
    Route::get('reports', 'ReportsController@index');
    Route::get('reports/invoices', 'ReportsController@invoices');
    Route::get('reports/invoices/export/excel', 'ReportsController@ExportInvoicesExcel');
    Route::get('reports/invoices/export/pdf', 'ReportsController@ExportInvoicesPDF');

    Route::get('reports/payments', 'ReportsController@payments');
    Route::get('reports/payments/export/excel', 'ReportsController@ExportPaymentsExcel');
    Route::get('reports/payments/export/pdf', 'ReportsController@ExportPaymentsPDF');

    Route::get('reports/summary', 'ReportsController@yearlySummary');

    #Admin
    Route::post('admin/staff/create', 'ManageStaffController@create')->name('create');
    Route::get('admin/staff/reset-password', 'ManageStaffController@PasswordReset');
    Route::post('admin/staff/reset-password', 'ManageStaffController@SavePasswordReset');

    Route::get('admin/staff', 'ManageStaffController@index')->name('staff');
    Route::get('admin/staff/edit-staff/{any}', 'ManageStaffController@index')->name('staff');
    Route::get('admin/staff/{any}', 'ManageStaffController@index')->name('staff');

    #Audit
    Route::get('admin/audit/payments', 'PaymentController@ViewPaymentsAudit');
});
