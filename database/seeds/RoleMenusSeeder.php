<?php

use Illuminate\Database\Seeder;

class RoleMenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'role_id' => '4',
                'menu_id' => '1',
            ],
            [
                'role_id' => '4',
                'menu_id' => '2',
            ],
            [
                'role_id' => '1',
                'menu_id' => '1',
            ],

            [
                'role_id' => '1',
                'menu_id' => '2',
            ],

        ];

        foreach ($menus as $menu)
            DB::table('role_menus')->insert($menu);
    }
}
