<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'name' => 'Profile',
                'url' => '/profile',
                'menu_category_id' => '1',
                'priority' => '1',
            ],
            [
                'name' => 'Change Password',
                'url' => '/change-password',
                'menu_category_id' => '1',
                'priority' => '2',
            ],

        ];

        foreach ($menus as $menu)
            DB::table('menus')->insert($menu);
    }
}
