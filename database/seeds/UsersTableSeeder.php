<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Smith',
            'email' => 'admin@sp.com',
            'role_id' => '4',
            'password' => bcrypt('secret'),
        ]);
    }
}
