<?php

use Illuminate\Database\Seeder;

class MenuCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menuCategories = [
            [
                'name' => 'My Account',
                'icon' => 'fa fa-bar-chart',
                'priority' => '1',
            ],
            [
                'name' => 'Payments',
                'icon' => 'fa fa-bar-chart',
                'priority' => '2',
            ],
            [
                'name' => 'Expenses',
                'icon' => 'fa fa-bar-chart',
                'priority' => '4',
            ],
            [
                'name' => 'Invoices',
                'icon' => 'fa fa-bar-chart',
                'priority' => '3',
            ],
            [
                'name' => 'Users',
                'icon' => 'fa fa-bar-chart',
                'priority' => '5',
            ],
            [
                'name' => 'Menus',
                'icon' => 'fa fa-bar-chart',
                'priority' => '3',
            ],
            [
                'name' => 'Settings',
                'icon' => 'fa fa-bar-chart',
                'priority' => '6',
            ],

        ];

        foreach ($menuCategories as $cat)
            DB::table('menu_categories')->insert($cat);
    }
}
