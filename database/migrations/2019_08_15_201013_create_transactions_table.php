<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->string('fullname');
            $table->string('address');
            $table->string('phone');
            $table->integer('customer_id')->nullable();
            $table->integer('invoice_id')->nullable();
            $table->string('initiator_id');
            $table->decimal('amount');
            $table->text('description')->nullable();
            $table->integer('transaction_type_id');
            $table->integer('transaction_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
