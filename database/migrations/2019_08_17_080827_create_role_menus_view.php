<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleMenusView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE  VIEW `vwmenuinrole`  AS  select `menus`.`id` AS `id`,`menus`.`menu_category_id` AS `menu_category_id`,`menus`.`name` AS `name`,`menus`.`url` AS `url`,`menus`.`priority` AS `priority`,`menu_categories`.`priority` AS `CategoryPriority`,`menus`.`activated` AS `activated`,`menus`.`displayed` AS `displayed`,`menu_categories`.`name` AS `MainMenu`,`menus`.`name` AS `Submenu`,`roles`.`id` AS `RoleId`,`roles`.`name` AS `RoleName` from (((`menus` join `menu_categories` on((`menu_categories`.`id` = `menus`.`menu_category_id`))) join `role_menus` on((`menus`.`id` = `role_menus`.`menu_id`))) join `roles` on((`roles`.`id` = `role_menus`.`role_id`))) ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW vwmenuinrole");
    }
}
